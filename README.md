# Automatic E-Assessment Item Annotation based on Machine Learning techniques

This repository provides source code, trained models and raw data to train models for automatic item annotation for "Anforderungsstufen". It is based on the Masters thesis of Daniel Jenatschke and was developed by him.

Source code is written in Python and Java. Raw data is provided as a mysql dump and spreadsheet files.

## Folders
#### Results
Contains plain results (mysql dump) inside the `Database Files` folder, two spreadsheets with evaluation results and several gnuplot scripts to create visual plots from these results

#### Source Code
Contains the source code of the experiment, used to create the aforementioned results.

#### Service files
Contains python scripts and model files, which might be used to build a web service, providing an API to categorize Items for their "Anforderungsstufe".
