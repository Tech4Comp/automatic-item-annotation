package helpers;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.io.FileNotFoundException;
import java.io.IOException;

public class StopWordChecker {
	private String stopWordFile;
	private String[] stopwords;
	public StopWordChecker(String stopWordFile) {
		super();
		this.stopWordFile = stopWordFile;
		ReadStopWords();
	}
	
	private void ReadStopWords() {
		String line=null;
		String allText="";
		try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(
            	    new FileInputStream(stopWordFile), "UTF-8"));

            while((line = bufferedReader.readLine()) != null) {
                allText+=line;
            }   
            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                		stopWordFile + "'"); 
            System.exit(0);
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + stopWordFile + "'");                  
        }
		allText=allText.toLowerCase();
		stopwords=allText.split(",");
	}
	
	public Boolean checkStopword(String input) {
		if (Arrays.asList(stopwords).contains(input.toLowerCase())) {
			return true;
		} else {
			return false;
		}
		
	}
	
	
	
	
}
