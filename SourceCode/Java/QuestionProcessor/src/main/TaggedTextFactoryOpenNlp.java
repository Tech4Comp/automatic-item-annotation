package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import helpers.StopWordChecker;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer.ALGORITHM;
import opennlp.tools.tokenize.Tokenizer;
import opennlp.tools.tokenize.TokenizerME;
import opennlp.tools.tokenize.TokenizerModel;
import taggedEntities.PreprocessedSentence;
import taggedEntities.PreprocessedText;
import taggedEntities.TaggedWord;

public class TaggedTextFactoryOpenNlp {
	  // Hier sind die Pfade zu den Modellen zu hinterlegen
    static String pathToSenModel="./res/opennlp/de-sent.bin";
    static String pathToTokenModel="./res/opennlp/de-token.bin";
    static String pathToPosMaxEntModel="./res/opennlp/de-pos-maxent.bin";
    static String pathToPosPreceptronModel="./res/opennlp/de-pos-perceptron.bin";
    
    // Diese Modelle werden f�r die Textverarbeitung verwendet
    static SentenceModel senModel=null;
    static TokenizerModel toModel=null;
    static POSModel posMaxEModel=null;
    static POSModel posPreceptronModel=null;
    
    // Die Objekte verarbeiten den Text mit den oben konfigurierten Modellen
    static SentenceDetectorME sentenceDetector=null;
    static Tokenizer tokenizer=null;
    static POSTaggerME posMaxEntTagger=null;
    static POSTaggerME posPreceptronTagger=null;
    
    // Statistik Fabriken
    static StopWordChecker stopWordChecker=new StopWordChecker("./res/stopwords-de.txt");
    static SnowballStemmer snowballStemmer= new SnowballStemmer(ALGORITHM.GERMAN);
    
    static {
		getSentenceDetector();
		getTokenizor();
		getPosMaxEntTagger();
		getposPreceptronTagger();
    }
    
    public PreprocessedText tagText(String inputText) {
		// Trennt die einzelnen S�tze des Arrays voneinadner ab
		String[] sentences=sentenceDetector.sentDetect(inputText);
		List<PreprocessedSentence>preprocessedSentences=new ArrayList<PreprocessedSentence>();
		for (int i = 0; i < sentences.length; i++) {
			PreprocessedSentence taggedSentence= new PreprocessedSentence();
			// Teilt den Satz in seine Token
			String[] words=tokenizer.tokenize(sentences[i]);
			String[] tags=posMaxEntTagger.tag(words);
			//double[] probs=posMaxEntTagger.probs();
			for (int j = 0; j < words.length; j++) {
				Boolean stopword=stopWordChecker.checkStopword(words[j]);
				String stemm=snowballStemmer.stem(words[j]).toString();
				TaggedWord taggedWord= new TaggedWord(words[j], j+1, tags[j], stemm, stopword);
				//taggedWord.setLemma(snowballStemmer.stem(words[j]).toString());
				taggedSentence.addTaggedWord(taggedWord);
			}
			taggedSentence.setSentence(sentences[i]);
			//taggedSentence.setSentenceStatistic(sentenceStatisticFactory.generateSentenceStatistic(taggedSentence));
			preprocessedSentences.add(taggedSentence);
			System.out.println(taggedSentence);
		}
		PreprocessedText taggedText=new PreprocessedText(preprocessedSentences);
		//taggedText.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedText));
		return taggedText;
	}
    
 // Initialisieren eines Sentence Detector Objektes
    private static void getSentenceDetector(){
        try {
           InputStream senModelIn= new FileInputStream(pathToSenModel);          
           try {
               senModel=new SentenceModel(senModelIn);
               senModelIn.close();
               sentenceDetector = new SentenceDetectorME(senModel);
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       } catch (FileNotFoundException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }       
   }
   // Initialisieren eines Tokenizer Objektes
   private static void getTokenizor(){
       try {
           InputStream toModelIn= new FileInputStream(pathToTokenModel);          
           try {
               toModel=new TokenizerModel(toModelIn);
               toModelIn.close();
               tokenizer=new TokenizerME(toModel);
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       } catch (FileNotFoundException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }       
    }
   // Initialiseren eines POS Tagger bassierend auf einem Maximum Entity Modells
   private static void getPosMaxEntTagger(){
       try {
           InputStream posTagerModelIn= new FileInputStream(pathToPosMaxEntModel);          
           try {
               posMaxEModel=new POSModel(posTagerModelIn);
               posTagerModelIn.close();
               posMaxEntTagger=new POSTaggerME(posMaxEModel);
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       } catch (FileNotFoundException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }       
   }
   // Initialiseren eines POS Tagger bassierend auf einem Preceptron Modells
   private static void getposPreceptronTagger(){
       try {
           InputStream posPrecptronModelIn= new FileInputStream(pathToPosPreceptronModel);          
           try {
               posPreceptronModel=new POSModel(posPrecptronModelIn);
               posPrecptronModelIn.close();
               posPreceptronTagger=new POSTaggerME(posPreceptronModel);
           } catch (IOException ex) {
               Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
           }
       } catch (FileNotFoundException ex) {
           Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
       }       
   }
}
