package main;

import java.util.ArrayList;
import java.util.List;
import org.annolab.tt4j.TreeTaggerWrapper;

import entities.Item;
import taggedEntities.PreprocessedItem;
import taggedEntities.TaggedWord;

public class TaggedItemFactoryTreeTagger {
	
	static TreeTaggerWrapper<String> treeTagger;
	List<TaggedWord> taggedWords=new ArrayList<TaggedWord>();
	public TaggedItemFactoryTreeTagger() {
	}
	
	public PreprocessedItem tagItem(Item item) {		
		PreprocessedItem taggedItem=new PreprocessedItem(item);
		TaggedTextFactoryTreeTagger taggedTextFactoryTreeTagger=new TaggedTextFactoryTreeTagger();
		taggedItem.setDescription(taggedTextFactoryTreeTagger.tagText(item.getDescription()));
		taggedItem.setQuestion(taggedTextFactoryTreeTagger.tagText(item.getQuestion()));
		taggedItem.setFullText(taggedTextFactoryTreeTagger.tagText(item.getQuestion()+" "+item.getDescription()));
		taggedItem.setLevel(item.getLevel());
		taggedItem.setType(item.getType());
		taggedItem.setId(item.getEalid());
		//taggedItem.setItemStatistic(itemStatisticFactory.generateItemStatistic(taggedItem));
		return taggedItem;
	}

}
