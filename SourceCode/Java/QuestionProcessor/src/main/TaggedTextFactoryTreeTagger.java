package main;

import java.io.IOException;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.List;

import org.annolab.tt4j.TokenHandler;
import org.annolab.tt4j.TreeTaggerException;
import org.annolab.tt4j.TreeTaggerWrapper;

import helpers.StopWordChecker;
import statistic.SentenceStatistic;
import statistic.TextStatistic;
import taggedEntities.PreprocessedSentence;
import taggedEntities.PreprocessedText;
import taggedEntities.TaggedWord;

public class TaggedTextFactoryTreeTagger {
	static TreeTaggerWrapper<String> treeTagger;
	static List<TaggedWord> taggedWords=new ArrayList<TaggedWord>();
	static StopWordChecker stopWordChecker=new StopWordChecker("./res/stopwords-de.txt");
	static {
		String inputText="";
		
		System.setProperty("treetagger.home", "res/treetagger");
		treeTagger=new TreeTaggerWrapper<String>();
		try {
			treeTagger.setModel("/german.par");
			treeTagger.setHandler(new TokenHandler<String>() {
				public void token(String token, String pos, String lemma) {
					Boolean stopword=stopWordChecker.checkStopword(token);
					TaggedWord taggedWord=new TaggedWord(token, pos, lemma, stopword);				
					taggedWords.add(taggedWord);
				}
			});			
		} catch (Exception e) {
			// Fehler bei der Initialisierung abfangen und ausgeben
			System.out.println(e.getMessage());
		}
		List<String> tokens=tokenize(inputText);
		try {
			treeTagger.process(tokens);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TreeTaggerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public TaggedTextFactoryTreeTagger() {
	}

	public PreprocessedText tagText (String inputText) {
		List<String> tokens=tokenize(inputText);
		List<PreprocessedSentence> taggedSentences=new ArrayList<PreprocessedSentence>();
		try {
			treeTagger.process(tokens);
			if (taggedWords.size()>0) {
				PreprocessedSentence taggedSentence= new PreprocessedSentence();
				int position=1;
				for (TaggedWord taggedWord : taggedWords) {
					taggedWord.setPosition(position);
					taggedSentence.addTaggedWord(taggedWord);
					//System.out.println(taggedSentence.toString());
					if (taggedWord.getPosTag()=="$.") {
						position=1;
						taggedSentence.generateSentenceFromTaggedWords();
						//System.out.println(taggedSentence.toString());
						//taggedSentence.setSentenceStatistic(sentenceStatisticFactory.generateSentenceStatistic(taggedSentence));
						taggedSentences.add(taggedSentence);
						//System.out.println(taggedSentence.getSentence());					
						taggedSentence = new PreprocessedSentence();
					} else {
						position ++;
						//System.out.println(position);
					}
				}				
			}		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (TreeTaggerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		taggedWords.clear();
		//System.out.println(taggedSentences.size());
		PreprocessedText taggedText=new PreprocessedText(taggedSentences);
		for (int i = 0; i < taggedSentences.size(); i++) {
			//SentenceStatisticFactory sentenceStatisticFactory= new SentenceStatisticFactory();
			//SentenceStatistic sentenceStatistic= new SentenceStatistic();
			//sentenceStatistic=sentenceStatisticFactory.generateSentenceStatistic(taggedSentences.get(i));
			//taggedSentences.get(i).setSentenceStatistic(sentenceStatistic);
			//System.out.println(taggedSentences.get(i));
		}
		//TextStatisticFactory textStatisticFactory= new TextStatisticFactory();
		//TextStatistic textStatistic= new TextStatistic();
		//textStatistic=textStatisticFactory.generateTextStatistic(taggedText);
		//taggedText.setTextStatistic(textStatistic);
		//System.out.println(taggedText.getTaggedSentences().get(taggedSentences.size()-2).getSentenceStatistic());
		//System.out.println(taggedText.toString());
		//System.out.println(taggedText.getTaggedSentences().get(0).toString());
		return taggedText;
		//return TaggedText;
	}
	
	// Tokenizer
	private static List<String> tokenize(
			final String aString)
	{
		List<String> tokens = new ArrayList<String>();
		BreakIterator bi = BreakIterator.getWordInstance();
		bi.setText(aString.toString());
		int begin = bi.first();
		int end;
		for (end = bi.next(); end != BreakIterator.DONE; end = bi.next()) {
			String t = aString.substring(begin, end);
			if (t.trim().length() > 0) {
				tokens.add(aString.substring(begin, end));
			}
			begin = end;
		}
		if (end != -1) {
			tokens.add(aString.substring(end));
		}
		return tokens;
	}

	
}
