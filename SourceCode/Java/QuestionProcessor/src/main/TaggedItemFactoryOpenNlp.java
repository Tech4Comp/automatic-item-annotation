package main;



import entities.Item;
import helpers.StopWordChecker;
import taggedEntities.PreprocessedItem;

public class TaggedItemFactoryOpenNlp {
	public PreprocessedItem tagItem(Item item) {
		
		PreprocessedItem taggedItem=new PreprocessedItem(item);
		/*
		taggedItem.setDescription(tagText(item.getDescription()));
		taggedItem.setQuestion(tagText(item.getQuestion()));*/
		TaggedTextFactoryOpenNlp taggedTextFactoryOpenNlp=new TaggedTextFactoryOpenNlp();
		taggedItem.setQuestion(taggedTextFactoryOpenNlp.tagText(item.getQuestion()));
		taggedItem.setDescription(taggedTextFactoryOpenNlp.tagText(item.getDescription()));
		taggedItem.setFullText(taggedTextFactoryOpenNlp.tagText(item.getQuestion()+" "+item.getDescription()));
		taggedItem.setLevel(item.getLevel());
		taggedItem.setType(item.getType());
		taggedItem.setId(item.getEalid());
		//taggedItem.setItemStatistic(taggedItemStatisticFactory.generateItemStatistic(taggedItem));
		return taggedItem;
	}
}
