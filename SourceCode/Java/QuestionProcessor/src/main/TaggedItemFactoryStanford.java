package main;

import entities.Item;
import taggedEntities.PreprocessedItem;

public class TaggedItemFactoryStanford {

	public TaggedItemFactoryStanford() {
	}
	
	public PreprocessedItem tagItem(Item item) {
		PreprocessedItem taggedItem=new PreprocessedItem(item);
		TaggedTextFactoryStanford taggedTextFactory=new TaggedTextFactoryStanford();
		taggedItem.setDescription(taggedTextFactory.tagText(item.getDescription()));
		taggedItem.setQuestion(taggedTextFactory.tagText(item.getQuestion()));
		//taggedItem.setItemStatistic(itemStaisticFactory.generateItemStatistic(taggedItem));
		taggedItem.setFullText(taggedTextFactory.tagText(item.getQuestion()+" "+item.getDescription()));
		taggedItem.setLevel(item.getLevel());
		taggedItem.setType(item.getType());
		taggedItem.setId(item.getEalid());
		return taggedItem;
	}
}
