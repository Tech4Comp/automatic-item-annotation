package main;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;

import entities.Answer;
import entities.Item;
import excelExport.ExcelStatisticExporter;
import helpers.StopWordChecker;
import statistic.ListItemStatistic;
import statistic.TextStatistic;
import taggedEntities.PreprocessedItem;
import taggedEntities.PreprocessedText;

public class Main {
	public static void main(String[] args) {
		
		
		// Einen Modus festlegen
		//System.out.println("Was m�chten sie analysieren? \n 1 Items \n 2 Textdatei");
		String modus="1";
		if (modus=="" || modus.isEmpty()) {
			modus="1";
		}
		
		// Einen Pfad zur Inputdatei festlegen, wenn dieser leer ist Standardpfad
		System.out.println("CSV oder Textdatei Datei einlesen (absoluter Pfad - Standard: C:\\temp\\Items.csv):");
		String pathToInputFile =getStringFromCommandLine();
		if (pathToInputFile=="" || pathToInputFile.isEmpty()) {
			if (modus.equals("1")) {
				pathToInputFile="C:\\temp\\Items.csv";
			} else {
				pathToInputFile="C:\\temp\\Text.txt";
			}
		}
		// Den Inputtext entgegennehmen 
		String inputText="";		
		try {
			inputText=convertStreamToString(new FileInputStream(pathToInputFile));			
			//inputText=Jsoup.clean(inputText, Whitelist.none());
			inputText=clean(inputText);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		// Einen Pfad f�r die Outputdateien eingeben
		System.out.println("Einen Pfad zur ablage der Outputdateien Festlegen (absoltuer Pfad - Standrad: Projektverzeichnis)");
		String outDir =getStringFromCommandLine();
		if (outDir=="" || outDir.isEmpty()) {
			outDir=".\\";
		}
		
		// Einen Pfad f�r die Outputdateien eingeben
		//System.out.println("Geben sie das Format der CSV-Datei an: \n"
			//	+ "1 [id, level, type, title, question, points, minnumber, maxnumber]");
		//String csvFormat =getStringFromCommandLine();
		//if (csvFormat=="" || csvFormat.isEmpty()) {
			//csvFormat="1";
		//}
		String csvFormat="1";
		
		// Userwahl, welcher Tagger verwendet werden soll
		System.out.println("Welchen Taggger Wollen sie Verwenden? \n 1 OpenNLP \n 2 TreeTagger \n 3 Stanford");
		String modelToUse =getStringFromCommandLine();
		// Hier werdeb sp�ter die genutzten Tagger gespeichert
		List<String> tagger= new ArrayList<String>();
		
		// Starten der Verarbeitung von Items
		if (modus.equals("1")) {
			System.out.println("Item Analyse gestartet");
			// Listen f�r getaggte Items initialisieren
			List<Item> items=new ArrayList<Item>();
			List<PreprocessedItem> taggedItems=new ArrayList<PreprocessedItem>();
			List<PreprocessedItem> taggedItemsOpenNlp=new ArrayList<PreprocessedItem>();
			List<PreprocessedItem> taggedItemsStanford=new ArrayList<PreprocessedItem>();
			List<PreprocessedItem> taggedItemsTreeTagger=new ArrayList<PreprocessedItem>();
			
			// Die Fabriken zum Taggen initialiseren
			TaggedItemFactoryOpenNlp taggedItemFactoryOpenNlp= new TaggedItemFactoryOpenNlp();
			TaggedItemFactoryTreeTagger taggedItemFactoryTreeTagger= new TaggedItemFactoryTreeTagger();
			TaggedItemFactoryStanford taggedItemFactoryStanford= new TaggedItemFactoryStanford();
			
			
			//Einlesen der Items
			String[] lines=inputText.split("[$]{3}");	
			for (int i = 0; i < lines.length; i++) {
				//System.out.println(lines[i]);
				String[] line = lines[i].split("[$]{1}");
				System.out.println(line.length);
				if(csvFormat=="1") {
					if(line.length>=8) {
						if(i>0) {
							System.out.println(line[5]);
							Item item =new Item(line[2], line[0], line[3],"", line[4], Integer.parseInt(line[5]), line[1]);
							System.out.println(item.toString());
							items.add(item);
						}						
					}
				} else if(csvFormat=="2")
				{
					if (line.length!=20) {
							System.out.println(lines[i]);
							/*Item item= new Item(line[0], line[1], line[2], line[3], line[4], Integer.parseInt(line[5]));				
							List<Answer> answers= new ArrayList<Answer>();
							answers.add(new Answer(line[6], Integer.parseInt(line[7])));
							answers.add(new Answer(line[6], Integer.parseInt(line[7])));
							answers.add(new Answer(line[6], Integer.parseInt(line[7])));
							answers.add(new Answer(line[6], Integer.parseInt(line[7])));
							answers.add(new Answer(line[6], Integer.parseInt(line[7])));
							item.setAnswers(answers);
							System.out.println(item.toString());
							items.add(item);*/
					}
				}
			}
		
			
		// Je nach User-Auswahl mit einer Fabrik getaggte Items produzieren + das Model bennen
		String modelName="";
		for (int i = 0; i < items.size(); i++) {
			PreprocessedItem taggedItem= null;
			switch (modelToUse) {
			case "1":
				taggedItem = taggedItemFactoryOpenNlp.tagItem(items.get(i));	
				taggedItems.add(taggedItem);
				modelName="OpenNLP";
				break;
			case "2":
				taggedItem=taggedItemFactoryTreeTagger.tagItem(items.get(i));
				taggedItems.add(taggedItem);
				modelName="TreeTagger";
				break;
			case "3":
				taggedItem=taggedItemFactoryStanford.tagItem(items.get(i));
				taggedItems.add(taggedItem);
				modelName="StanfordTagger";
				break;
			case "4":
				taggedItem=taggedItemFactoryStanford.tagItem(items.get(i));
				taggedItemsStanford.add(taggedItem);
				taggedItemsOpenNlp.add(taggedItemFactoryOpenNlp.tagItem(items.get(i)));
				taggedItemsTreeTagger.add(taggedItemFactoryTreeTagger.tagItem(items.get(i)));
				break;
			default:
				break;
			}			
				

			}
		System.out.println(taggedItems.toString());
		ObjectMapper mapper = new ObjectMapper();
		OutputStreamWriter writer=null;
		try {
				writer =new OutputStreamWriter(new FileOutputStream(outDir+modelName+".json"));
		    // do stuff
		} catch (Exception e) {
			System.out.println(e);
		}
	        /**
	         * Write object to file
	         */
	        try {
	            mapper.writeValue(writer, taggedItems);//Plain JSON
	            //mapper.writerWithDefaultPrettyPrinter().writeValue(new File("result.json"), carFleet);//Prettified JSON
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		}
		/*
			// Je nach User Wahl die Statistik der List produzieren
			ListItemStatisticFactory listItemStatisticFactory= new ListItemStatisticFactory();
			List<ListItemStatistic> listItemStatistics= new ArrayList<ListItemStatistic>();	
			ListItemStatistic listItemStatistic=new ListItemStatistic();
			switch (modelToUse) {
			case "1":
				listItemStatistic= listItemStatisticFactory.generateListItemsStatistic(taggedItems);
				listItemStatistic.orderMapsByCount();
				listItemStatistics.add(listItemStatistic);
				tagger.add("OpenNlp");
				System.out.println(listItemStatistic.toString());
				break;
			case "2":
				listItemStatistic= listItemStatisticFactory.generateListItemsStatistic(taggedItems);
				listItemStatistic.orderMapsByCount();
				listItemStatistics.add(listItemStatistic);
				tagger.add("TreeTagger");
				System.out.println(listItemStatistic.toString());
				break;
			case "3":
				listItemStatistic= listItemStatisticFactory.generateListItemsStatistic(taggedItems);
				listItemStatistic.orderMapsByCount();
				listItemStatistics.add(listItemStatistic);
				tagger.add("Stanford");
				System.out.println(listItemStatistic.toString());
				break;
			case "4":
				listItemStatistics.add(listItemStatisticFactory.generateListItemsStatistic(taggedItemsOpenNlp));
				tagger.add("OpenNlp");
				System.out.println("OpenNlp: \n");
				System.out.println(listItemStatistics.get(0).toString());
				listItemStatistics.add(listItemStatisticFactory.generateListItemsStatistic(taggedItemsTreeTagger));
				tagger.add("TreeTagger");
				System.out.println("TreeTagger: \n");
				System.out.println(listItemStatistics.get(1).toString());
				listItemStatistics.add(listItemStatisticFactory.generateListItemsStatistic(taggedItemsStanford));
				tagger.add("Stanford");
				System.out.println("Stanford: \n");
				System.out.println(listItemStatistics.get(2).toString());
				break;
			default:
				break;
			}
			// Excelfile produzieren
			ExcelStatisticExporter excelFileFactory= new ExcelStatisticExporter(outDir);
			excelFileFactory.createExcelFromListItemStatistic(tagger, listItemStatistics);	
			excelFileFactory=null;
			//ExcelItemExporter excelItemExporter= new ExcelItemExporter(outDir);
			//excelItemExporter.createExcelFileFromItemList(taggedItems);
			
			// Textfile mit den Items erstellen
			if(modelToUse.equals("1")||modelToUse.equals("2")||modelToUse=="3")
			{
				try {				
					System.out.println(modelName);
					PrintWriter out = new PrintWriter(outDir+"\\"+modelName+".txt");
					out.print(taggedItems.toString());
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
			else {
				try {
					//PrintWriter out = new PrintWriter(outDir+"\\TreeTagger.txt");
					PrintWriter out= new PrintWriter(new PrintWriter(outDir+"\\TreeTagger.txt"), true);
					out.println(taggedItemsTreeTagger.toString());
					out.close();
					//PrintWriter out2= new PrintWriter(outDir+"\\StanfordTagger.txt");
					PrintWriter out2= new PrintWriter(new PrintWriter(outDir+"\\StanfordTagger.txt"),true);
					out2.println(taggedItemsStanford.toString());
					out2.close();
					PrintWriter out3= new PrintWriter(new PrintWriter(outDir+"\\OpenNLP.txt"), true);
					out3.println(taggedItemsOpenNlp.toString());
					out3.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
			}
		}	
		// Verarbeitung von einfachen Texten
		else {
			// Die Texte definieren
			PreprocessedText taggedTextOpenNlp;
			PreprocessedText taggedTextTreeTagger;
			PreprocessedText taggedTextStanford;
			
			// Die Fabrikendefinieren
			TaggedTextFactoryOpenNlp taggedTextFactoryOpenNlp;
			TaggedTextFactoryStanford taggedTextFactoryStanford;
			TaggedTextFactoryTreeTagger taggedTextFactoryTreeTagger;
			//TextStatisticFactory textStatisticFactory= new TextStatisticFactory();
			ExcelStatisticExporter excelFileFactory= new ExcelStatisticExporter(outDir);
			List<TextStatistic>textStatistics= new ArrayList<TextStatistic>();
			
			switch (modelToUse) {
			case "1":		
				taggedTextFactoryOpenNlp= new TaggedTextFactoryOpenNlp();
				tagger.add("OpenNlp");
				taggedTextOpenNlp=taggedTextFactoryOpenNlp.tagText(inputText);
				//taggedTextOpenNlp.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextOpenNlp));
				textStatistics.add(taggedTextOpenNlp.getTextStatistic());
				try {				
					PrintWriter out = new PrintWriter(outDir+"\\"+"OpenNLP.txt");
					out.print(taggedTextOpenNlp.toString());
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				break;
			case "2":			
				taggedTextFactoryTreeTagger= new TaggedTextFactoryTreeTagger();
				tagger.add("TreeTagger");
				taggedTextTreeTagger=taggedTextFactoryTreeTagger.tagText(inputText);
				//taggedTextTreeTagger.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextTreeTagger));
				textStatistics.add(taggedTextTreeTagger.getTextStatistic());
				try {				
					PrintWriter out = new PrintWriter(outDir+"\\"+"TreeTagger.txt");
					out.print(taggedTextTreeTagger.toString());
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				break;
			case "3":
				taggedTextFactoryStanford= new TaggedTextFactoryStanford();
				tagger.add("Stanford");
				taggedTextStanford= taggedTextFactoryStanford.tagText(inputText);
				//taggedTextStanford.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextStanford));
				textStatistics.add(taggedTextStanford.getTextStatistic());
				try {				
					PrintWriter out = new PrintWriter(outDir+"\\"+"Stanford.txt");
					out.print(taggedTextStanford.toString());
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				break;
			case "4":

				tagger.add("OpenNlp");
				taggedTextFactoryOpenNlp= new TaggedTextFactoryOpenNlp();
				taggedTextOpenNlp=taggedTextFactoryOpenNlp.tagText(inputText);
				//taggedTextOpenNlp.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextOpenNlp));
				textStatistics.add(taggedTextOpenNlp.getTextStatistic());
				
				tagger.add("TreeTagger");			
				taggedTextFactoryTreeTagger= new TaggedTextFactoryTreeTagger();
				taggedTextTreeTagger=taggedTextFactoryTreeTagger.tagText(inputText);
				//taggedTextTreeTagger.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextTreeTagger));
				textStatistics.add(taggedTextTreeTagger.getTextStatistic());
				
				tagger.add("Stanford");
				taggedTextFactoryStanford= new TaggedTextFactoryStanford();
				taggedTextStanford= taggedTextFactoryStanford.tagText(inputText);
				//taggedTextStanford.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedTextStanford));
				textStatistics.add(taggedTextStanford.getTextStatistic());
				try {				
					PrintWriter out = new PrintWriter(outDir+"\\"+"OpenNLP.txt");
					out.print(taggedTextOpenNlp.toString());
					out.close();
					out = new PrintWriter(outDir+"\\"+"TreeTagger.txt");
					out.print(taggedTextTreeTagger.toString());
					out.close();
					out = new PrintWriter(outDir+"\\"+"Stanford.txt");
					out.print(taggedTextStanford.toString());
					out.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
				break;
			default:
				break;
			}
			excelFileFactory.createExcelFromTextStatisticList(tagger, textStatistics);
			
			
		}*/
	}
	
	// Methode um einen Usereingabe als String entgegen zu nehmen
	public static String getStringFromCommandLine() {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String text="";
        // Texteingabe
        try {
            text = reader.readLine();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        return text;
	}
	
	public static String clean(String input) {
		String output="";
		output=input.replace("&nbsp;", " ");
		output=output.replace("&amp;", " ");
		output=output.replace(" .", ".");
		output=output.replace("?", "? ");
		output=output.replace(".",". ");
		output=output.replace("  "," ");
		for (Map.Entry<String, String> abk : Abkuerzungen.abkuerzungen.entrySet()) {
			String replacement=abk.getKey();
			output=output.replace(replacement, abk.getValue());
			replacement= replacement.replace(" ", "(");
			output=output.replace(replacement, "("+abk.getValue().replace(" ", ""));
		}
		return output;
	}

	static String convertStreamToString(java.io.InputStream is) {
	    java.util.Scanner s = new java.util.Scanner(is,"UTF-8").useDelimiter("\\A");
	    return s.hasNext() ? s.next() : "";
	   
	}
	
}
