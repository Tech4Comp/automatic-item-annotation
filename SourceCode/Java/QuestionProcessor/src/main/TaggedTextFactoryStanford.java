package main;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.BasicConfigurator;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.CoreDocument;
import edu.stanford.nlp.pipeline.CoreSentence;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphEdge;
import edu.stanford.nlp.util.StringUtils;
import helpers.StopWordChecker;
import opennlp.tools.stemmer.snowball.SnowballStemmer;
import opennlp.tools.stemmer.snowball.SnowballStemmer.ALGORITHM;
import taggedEntities.SemanticRealationship;
import taggedEntities.PreprocessedSentence;
import taggedEntities.PreprocessedText;
import taggedEntities.TaggedWord;

public class TaggedTextFactoryStanford {
		static SnowballStemmer snowballStemmer= new SnowballStemmer(ALGORITHM.GERMAN);
		static StopWordChecker stopWordChecker=new StopWordChecker("./res/stopwords-de.txt");
		static StanfordCoreNLP pipeline;
		static {
			BasicConfigurator.configure();
			// Deutsches Sprachmodell einstellen
			Properties germanProperties = StringUtils.argsToProperties(
				new String[] {"-props","StanfordCoreNLP-german.properties"}
			);
			// Einstellen der auszuführenden Annotatoren
			germanProperties.setProperty("annotators", /*"tokenize,ssplit,pos,lemma,ner,parse,depparse,coref,kbp,quote"*/"tokenize,ssplit,pos,depparse");	
			pipeline=new StanfordCoreNLP(germanProperties);
		}
		public PreprocessedText tagText(String inputText) {
			CoreDocument document=new CoreDocument(inputText);
			pipeline.annotate(document);
			List<PreprocessedSentence> taggedSentences=new ArrayList<PreprocessedSentence>();
			List<CoreSentence> coreSentences= new ArrayList<CoreSentence>();
			coreSentences=document.sentences();
			// Durch die Sätze itterieren
			for (CoreSentence coreSentence : coreSentences) {
				PreprocessedSentence taggedSentence= new PreprocessedSentence();
				taggedSentence.setSentence(coreSentence.text());
				SemanticGraph tree= coreSentence.dependencyParse();
				List<SemanticGraphEdge>edges=tree.edgeListSorted();
				List<SemanticRealationship> semanticRealationships= new ArrayList<SemanticRealationship>();
				for (SemanticGraphEdge semanticGraphEdge : edges) {
					String source=snowballStemmer.stem(semanticGraphEdge.getSource().originalText()).toString();
					String relationship=semanticGraphEdge.getRelation().toString();
					String destination= snowballStemmer.stem(semanticGraphEdge.getTarget().originalText()).toString();
					semanticRealationships.add(new SemanticRealationship(source, relationship, destination));
				}
				List<CoreLabel>tokens=coreSentence.tokens();
				List<String> posTags=coreSentence.posTags();
				for (int i = 0; i < tokens.size(); i++) {
					String lemma= snowballStemmer.stem(tokens.get(i).originalText()).toString();
					Boolean stopword=stopWordChecker.checkStopword(tokens.get(i).originalText());
					TaggedWord taggedWord= new TaggedWord(tokens.get(i).originalText(), posTags.get(i), lemma, stopword);
					taggedWord.setPosition(i+1);
					taggedSentence.addTaggedWord(taggedWord);
				}
				
				//taggedSentence.setSemanticRealationships(semanticRealationships);
				//taggedSentence.setSentenceStatistic(sentenceStatisticFactory.generateSentenceStatistic(taggedSentence));
				taggedSentences.add(taggedSentence);
			}
			
			PreprocessedText taggedText= new PreprocessedText(taggedSentences);
			//taggedText.setTextStatistic(textStatisticFactory.generateTextStatistic(taggedText));
			return taggedText;		
		}
}
