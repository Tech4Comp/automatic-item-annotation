package entities;

public class Answer {
	private String text;
	private int points;
	public String getText() {
		return text;
	}
	
	public Answer(String text, int points) {
		super();
		this.text = text;
		this.points = points;
	}

	public void setText(String text) {
		this.text = text;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	@Override
	public String toString() {
		return "Answer [text=" + text + ", points=" + points + "]";
	}
	
}
