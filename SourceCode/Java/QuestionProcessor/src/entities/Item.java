package entities;

import java.util.List;

public class Item {
	private String type;
	private String ealid;
	private String title;
	private String description;
	private String question;
	private int points;
	private String level;
	
	private List<Answer> answers;

	public Item() {}
	
	public Item(String type, String ealid, String title, String description, String question, int points, String level) {
		super();
		this.type = type;
		this.ealid = ealid;
		this.title = title;
		this.description = description;
		this.question = question;
		this.points = points;
		this.level=level;
		answers=null;
	}
	
	public void addAnswer(Answer answ) {
		answers.add(answ);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getEalid() {
		return ealid;
	}

	public void setEalid(String ealid) {
		this.ealid = ealid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public List<Answer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Answer> answers) {
		this.answers = answers;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Override
	public String toString() {
		return "Item [type=" + type + ", ealid=" + ealid + ", title=" + title + ", description=" + description
				+ ", question=" + question + ", points=" + points + ", level=" + level + ", answers=" + answers + "]";
	}

	
	
	
	
	
	
}
