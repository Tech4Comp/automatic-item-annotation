package statistic;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import taggedEntities.SemanticRealationship;

public class ListItemStatistic {
	private int countToken=0;
	private int countSentence=0;
	private double tokenPerSentence=0;
	
	
	// Maps
	Map<String, Integer> lemma=new HashMap<String, Integer>();
	Map<String, Integer> posClasses=new HashMap<String, Integer>();
	Map<String, Integer> pos=new HashMap<String, Integer>();
	Map<SemanticRealationship, Integer> semanticRelationships= new HashMap<SemanticRealationship, Integer>();
	public int getCountToken() {
		return countToken;
	}
	public void setCountToken(int countToken) {
		this.countToken = countToken;
	}
	public int getCountSentence() {
		return countSentence;
	}
	public void setCountSentence(int countSentence) {
		this.countSentence = countSentence;
	}
	public double getTokenPerSentence() {
		return tokenPerSentence;
	}
	public void setTokenPerSentence(double tokenPerSentence) {
		this.tokenPerSentence = tokenPerSentence;
	}
	public Map<String, Integer> getLemma() {
		return lemma;
	}
	public void setLemma(Map<String, Integer> lemma) {
		this.lemma = lemma;
	}
	public Map<String, Integer> getPosClasses() {
		return posClasses;
	}
	public void setPosClasses(Map<String, Integer> posClasses) {
		this.posClasses = posClasses;
	}
	public Map<String, Integer> getPos() {
		return pos;
	}
	public void setPos(Map<String, Integer> pos) {
		this.pos = pos;
	}	
	public Map<SemanticRealationship, Integer> getSemanticRelationships() {
		return semanticRelationships;
	}
	public void setSemanticRelationships(Map<SemanticRealationship, Integer> semanticRelationships) {
		this.semanticRelationships = semanticRelationships;
	}
	public void orderMapsByCount() {
		lemma=lemma.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                LinkedHashMap::new));
		pos=pos.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                LinkedHashMap::new));
		posClasses=posClasses.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                LinkedHashMap::new));
		semanticRelationships=semanticRelationships.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(Collectors.toMap(e -> e.getKey(), e -> e.getValue(), (e1, e2) -> e2,
                LinkedHashMap::new));
	}
	
	@Override
	public String toString() {
		return "ListItemStatistic [countToken=" + countToken + "\n countSentence=" + countSentence
				+ "\n tokenPerSentence=" + tokenPerSentence + "\n lemma=" + lemma + "\n posClasses=" + posClasses
				+ "\n pos=" + pos + "\n Semantische Beziehungen=" + semanticRelationships + "]";
	}
	
	
}
