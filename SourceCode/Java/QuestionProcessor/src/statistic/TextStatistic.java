package statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

import taggedEntities.SemanticRealationship;

public class TextStatistic {
	//List<SentenceStatistic> sentenceStatistics = new ArrayList<SentenceStatistic>();
	int countSentence=0;
	int countToken=0;
	double tokenPerSentence=0.0;
	Map<String, Integer> pos=new HashMap<String, Integer>();
	Map<String, Integer> posClass=new HashMap<String, Integer>();
	Map<String, Integer> lemma=new HashMap<String, Integer>();
	Map<SemanticRealationship, Integer> semanticRelationships= new HashMap<SemanticRealationship, Integer>();
	
	/*public List<SentenceStatistic> getSentenceStatistic() {
		return sentenceStatistics;
	}
	public void setSentenceStatistic(List<SentenceStatistic> sentenceStatistic) {
		this.sentenceStatistics = sentenceStatistic;
	}*/
	public int getCountSentence() {
		return countSentence;
	}
	public void setCountSentence(int countSentence) {
		this.countSentence = countSentence;
	}
	public int getCountToken() {
		return countToken;
	}
	public void setCountToken(int countToken) {
		this.countToken = countToken;
	}
	public double getTokenPerSentence() {
		return tokenPerSentence;
	}
	public void setTokenPerSentence(double tokenPerSentence) {
		this.tokenPerSentence = tokenPerSentence;
	}
	public Map<String, Integer> getPos() {
		return pos;
	}
	public void setPos(Map<String, Integer> pos) {
		this.pos = pos;
	}
	public Map<String, Integer> getPosClass() {
		return posClass;
	}
	public void setPosClass(Map<String, Integer> posClass) {
		this.posClass = posClass;
	}
	public Map<String, Integer> getLemma() {
		return lemma;
	}
	public void setLemma(Map<String, Integer> lemma) {
		this.lemma = lemma;
	}
	/*public void addSentenceStatistic(SentenceStatistic sentenceStatistic) {
		sentenceStatistics.add(sentenceStatistic);
	}	*/
	public Map<SemanticRealationship, Integer> getSemanticRelationships() {
		return semanticRelationships;
	}
	public void setSemanticRelationships(Map<SemanticRealationship, Integer> semanticRelationships) {
		this.semanticRelationships = semanticRelationships;
	}
	@Override
	public String toString() {
		return "TextStatistic [Text Statistik=" /*+ sentenceStatistics */+ ", countSentence=" + countSentence
				+ ", countToken=" + countToken + ", tokenPerSentence=" + tokenPerSentence + ", pos=" + pos
				+ ", posClass=" + posClass + ", lemma=" + lemma +/* ", semanticRelationships=" + semanticRelationships
				+*/ "]";
	}
	
	
	
	
	
	
}
