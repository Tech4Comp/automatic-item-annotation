package statistic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.WeakHashMap;

import org.apache.commons.lang3.tuple.Pair;

import taggedEntities.SemanticRealationship;

public class SentenceStatistic {
	int countToken;
	private Map<String, Integer> posTags= new HashMap<String, Integer>();
	private List<Pair<String, Integer>> pos2= new ArrayList<Pair<String, Integer>>();
	private Map<String, Integer> posClass= new HashMap<String, Integer>();
	private Map<String, Integer> lemma= new HashMap<String, Integer>();
	private Map<SemanticRealationship, Integer> semanticRelationships= new HashMap<SemanticRealationship, Integer>();
	
	public SentenceStatistic(int countToken, Map<String, Integer> pos, Map<String, Integer> posClass,
			Map<String, Integer> lemma, Map<SemanticRealationship, Integer> semanticRelationships) {
		super();
		this.countToken = countToken;
		this.posTags = pos;
		this.posClass = posClass;
		this.lemma = lemma;
		this.semanticRelationships= semanticRelationships;
	}
	
	public SentenceStatistic() {}
	
	
	
	public List<Pair<String, Integer>> getPos2() {
		return pos2;
	}

	public void setPos2(List<Pair<String, Integer>> pos2) {
		this.pos2 = pos2;
	}

	public int getCountToken() {
		return countToken;
	}
	public void setCountToken(int countToken) {
		this.countToken = countToken;
	}
	public Map<String, Integer> getPos() {
		return posTags;
	}
	public void setPos(Map<String, Integer> pos) {
		this.posTags = pos;
	}
	public Map<String, Integer> getPosClass() {
		return posClass;
	}
	public void setPosClass(Map<String, Integer> posClass) {
		this.posClass = posClass;
	}
	public Map<String, Integer> getLemma() {
		return lemma;
	}
	public void setLemma(Map<String, Integer> lemma) {
		this.lemma = lemma;
	}

	public Map<SemanticRealationship, Integer> getSemanticRelationships() {
		return semanticRelationships;
	}

	public void setSemanticRelationships(Map<SemanticRealationship, Integer> semanticRelationships) {
		this.semanticRelationships = semanticRelationships;
	}

	@Override
	public String toString() {
		return "SentenceStatistic [countToken=" + countToken + ", pos=" + posTags + ", posClass=" + posClass + ", lemma="
				+ lemma + ", semanticRelationships=" + semanticRelationships + "]";
	}

	
	
	
}
