package statistic;

import java.util.Map;

public class ItemStatistic_old {
	private int countSentence;
	private int countSentenceQuestion;
	private int countSentenceDescription;
	
	private int countToken;
	private int countTokenQuestion;
	private int countTokenDescription;
	
	private double tokenSentence;
	private double tokenSentenceQuestion;
	private double tokenSentenceDescription;
	
	Map<String, Integer> posQuestion;
	Map<String, Integer> posDescription;
	Map<String, Integer> pos;
	Map<String, Integer> posClassQuestion;
	Map<String, Integer> posClassDescription;
	Map<String, Integer> posClass;
	Map<String, Integer> lemma;
	Map<String, Integer> lemmaQuestion;
	Map<String, Integer> lemmaDescription;
	
	/* Folgender Ansatz ist w�hrend der n�chsten Programmiersession zu verfolgen:
	Die TaggedItem und die Itemstatistic Factory sollen so ver�ndert werden, dass auch eine TaggedText und eine TextStatistic Factory zur Verf�gung steht. 
	Eventuel Abstarkte Klassen f�r TextStatistic / TaggedText Factory
	*/
	// Getter und Setter
	public int getCountSentence() {
		return countSentence;
	}
	public void setCountSentence(int countSentence) {
		this.countSentence = countSentence;
	}
	public int getCountSentenceQuestion() {
		return countSentenceQuestion;
	}
	public void setCountSentenceQuestion(int countSentenceQuestion) {
		this.countSentenceQuestion = countSentenceQuestion;
	}
	public int getCountSentenceDescription() {
		return countSentenceDescription;
	}
	public void setCountSentenceDescription(int countSentenceDescription) {
		this.countSentenceDescription = countSentenceDescription;
	}
	public int getCountToken() {
		return countToken;
	}
	public void setCountToken(int countToken) {
		this.countToken = countToken;
	}
	public int getCountTokenQuestion() {
		return countTokenQuestion;
	}
	public void setCountTokenQuestion(int countTokenQuestion) {
		this.countTokenQuestion = countTokenQuestion;
	}
	public int getCountTokenDescription() {
		return countTokenDescription;
	}
	public void setCountTokenDescription(int countTokenDescription) {
		this.countTokenDescription = countTokenDescription;
	}
	public double getTokenSentence() {
		return tokenSentence;
	}
	public void setTokenSentence(double tokenSentence) {
		this.tokenSentence = tokenSentence;
	}
	public double getTokenSentenceQuestion() {
		return tokenSentenceQuestion;
	}
	public void setTokenSentenceQuestion(double tokenSentenceQuestion) {
		this.tokenSentenceQuestion = tokenSentenceQuestion;
	}
	public double getTokenSentenceDescription() {
		return tokenSentenceDescription;
	}
	public void setTokenSentenceDescription(double tokenSentenceDescription) {
		this.tokenSentenceDescription = tokenSentenceDescription;
	}
	public Map<String, Integer> getPosQuestion() {
		return posQuestion;
	}
	public void setPosQuestion(Map<String, Integer> posQuestion){
		this.posQuestion = posQuestion;
		}
	public Map<String, Integer> getPosDescription() {
		return posDescription;
	}
	public void setPosDescription(Map<String, Integer> posDescription) {
		this.posDescription = posDescription;
	}
	public Map<String, Integer> getPos() {
		return pos;
	}
	public void setPos(Map<String, Integer> pos) {
		this.pos = pos;
	}
	public Map<String, Integer> getPosClassQuestion() {
		return posClassQuestion;
	}
	public void setPosClassQuestion(Map<String, Integer> posClassQuestion) {
		this.posClassQuestion = posClassQuestion;
	}
	public Map<String, Integer> getPosClassDescription() {
		return posClassDescription;
	}
	public void setPosClassDescription(Map<String, Integer> posClassDescription) {
		this.posClassDescription = posClassDescription;
	}
	public Map<String, Integer> getPosClass() {
		return posClass;
	}
	public void setPosClass(Map<String, Integer> posClass) {
		this.posClass = posClass;
	}
	
	public Map<String, Integer> getLemma() {
		return lemma;
	}
	public void setLemma(Map<String, Integer> lemma) {
		this.lemma = lemma;
	}
	public Map<String, Integer> getLemmaQuestion() {
		return lemmaQuestion;
	}
	public void setLemmaQuestion(Map<String, Integer> lemmaQuestion) {
		this.lemmaQuestion = lemmaQuestion;
	}
	public Map<String, Integer> getLemmaDescription() {
		return lemmaDescription;
	}
	public void setLemmaDescription(Map<String, Integer> lemmaDescription) {
		this.lemmaDescription = lemmaDescription;
	}
	
	@Override
	public String toString() {
		return "ItemStatistic [countSentence=" + countSentence + ", countSentenceQuestion=" + countSentenceQuestion
				+ ", countSentenceDescription=" + countSentenceDescription + ", countToken=" + countToken
				+ ", countTokenQuestion=" + countTokenQuestion + ", countTokenDescription=" + countTokenDescription
				+ ", tokenSentence=" + tokenSentence + ", tokenSentenceQuestion=" + tokenSentenceQuestion
				+ ", tokenSentenceDescription=" + tokenSentenceDescription + ", posQuestion=" + posQuestion
				+ ", posDescription=" + posDescription + ", pos=" + pos + ", posClassQuestion=" + posClassQuestion
				+ ", posClassDescription=" + posClassDescription + ", posClass=" + posClass + ", lemma=" + lemma
				+ ", lemmaQuestion=" + lemmaQuestion + ", lemmaDescription=" + lemmaDescription + "]";
	}
	
	
	
}
