package statistic;

import java.util.HashMap;
import java.util.Map;

import taggedEntities.SemanticRealationship;

public class ItemStatistic {
	// 
	private int countToken=0;
	private int countSentence=0;
	private double tokenPerSentence=0;
	
	
	// Maps
	Map<String, Integer> lemma=new HashMap<String, Integer>();
	Map<String, Integer> posClasses=new HashMap<String, Integer>();
	Map<String, Integer> pos=new HashMap<String, Integer>();
	Map<SemanticRealationship, Integer> semanticRelationships= new HashMap<SemanticRealationship, Integer>();
	public int getCountToken() {
		return countToken;
	}
	public void setCountToken(int countToken) {
		this.countToken = countToken;
	}
	public int getCountSentence() {
		return countSentence;
	}
	public void setCountSentence(int countSentence) {
		this.countSentence = countSentence;
	}
	public double getTokenPerSentence() {
		return tokenPerSentence;
	}
	public void setTokenPerSentence(double tokenPerSentence) {
		this.tokenPerSentence = tokenPerSentence;
	}
	public Map<String, Integer> getLemma() {
		return lemma;
	}
	public void setLemma(Map<String, Integer> lemma) {
		this.lemma = lemma;
	}
	public Map<String, Integer> getPosClasses() {
		return posClasses;
	}
	public void setPosClasses(Map<String, Integer> posClasses) {
		this.posClasses = posClasses;
	}
	public Map<String, Integer> getPos() {
		return pos;
	}
	public void setPos(Map<String, Integer> pos) {
		this.pos = pos;
	}
	
	public Map<SemanticRealationship, Integer> getSemanticRelationships() {
		return semanticRelationships;
	}
	public void setSemanticRelationships(Map<SemanticRealationship, Integer> semanticRelationships) {
		this.semanticRelationships = semanticRelationships;
	}
	@Override
	public String toString() {
		return "ItemStatistic2 [ \n \t countToken=" + countToken + ",\n \t countSentence=" + countSentence + ", \n \t tokenPerSentence="
				+ tokenPerSentence + ", \n \t lemma=" + lemma + ", \n \t posClasses=" + posClasses + ", \n \t pos=" + pos + ", \n \t Semantische Beziehungen=" + semanticRelationships + "]";
	}
	
	
	
}
