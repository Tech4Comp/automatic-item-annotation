package taggedEntities;

import entities.Item;
import statistic.ItemStatistic;

public class PreprocessedItem {
	Item item=new Item();
	
	PreprocessedText question= new PreprocessedText();
	PreprocessedText description= new PreprocessedText();
	PreprocessedText fullText=new PreprocessedText();
	String level;
	String type;
	ItemStatistic itemStatistic= new ItemStatistic();
	String itemType="";
	String id;
	
	
	public PreprocessedItem(Item item) {
		super();
		this.item = item;
	}

	

	public String getType() {
		return type;
	}



	public void setType(String type) {
		this.type = type;
	}



	//public Item getItem() {
		//return item;
	//}

	public void setItem(Item item) {
		this.item = item;
	}

	public PreprocessedText getQuestion() {
		return question;
	}

	public void setQuestion(PreprocessedText question) {
		this.question = question;
	}

	public PreprocessedText getDescription() {
		return description;
	}

	public void setDescription(PreprocessedText description) {
		this.description = description;
	}
	
	public void setItemStatistic(ItemStatistic itemStatistic) {
		// TODO Auto-generated method stub
		this.itemStatistic=itemStatistic;	
	}
	
	//public ItemStatistic getItemStatistic() {
	//	return itemStatistic;
//	}

	
	
	public PreprocessedText getFullText() {
		return fullText;
	}


	public void setFullText(PreprocessedText fullText) {
		this.fullText = fullText;
	}


	public String getItemType() {
		return itemType;
	}


	public void setItemType(String itemType) {
		this.itemType = itemType;
	}


	public String getId() {
		return id.replaceAll("(\r\n|\n)", " ");
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLevel() {
		return level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	@Override
	public String toString() {
		return "PreprocessedItem [question=" + question + ", description=" + description
				+ ", fullText=" + fullText + ", level=" + level /*+ ", itemStatistic=" + itemStatistic */+ ", itemType="
				+ itemType + ", id=" + id + "]";
	}


}
