package taggedEntities;

public class TaggedWord {
	   	private String word;
	    private int position;
	    private String posTag;
	    private String posTagClass;
	    private double prob;
	    private String secTag;
	    private double secTagProb;
	    private String lemma;
	    private Boolean stopword;
	    
	    /*public TaggedWord(String word, int position, String posTag, double prob, String secTag, double secTagProb,
				String lemma,  Boolean stopword) {
			super();
			this.word = word;
			this.position = position;
			this.posTag = posTag;
			this.prob = prob;
			this.secTag = secTag;
			this.secTagProb = secTagProb;
			this.lemma = lemma;
			this.stopword =stopword;
			generateTagClass();
			 
		}
	    

		public TaggedWord(String word, int position, String posTag, double prob) {
			super();
			this.word = word;
			this.position = position;
			this.posTag = posTag;
			this.prob = prob;
			generateTagClass();
		}*/
		
		public TaggedWord(String word, int position, String posTag, String lemma, Boolean stopword) {
			super();
			this.word = word;
			this.position = position;
			this.posTag = posTag;
			this.lemma = lemma;
			this.stopword=stopword;
			generateTagClass();
		}
		public TaggedWord(String word, String posTag, String lemma, Boolean stopword) {
			super();
			this.word = word;
			this.position = 0;
			this.posTag = posTag;
			this.lemma = lemma;
			this.stopword=stopword;
			generateTagClass();
		}
		/*
		public TaggedWord(String word, String posTag, String lemma) {
			super();
			this.word=word;
			this.posTag=posTag;
			this.lemma=lemma;
			generateTagClass();
		}*/

		
		
		public double getProb() {
	        return prob;
	    }

	    public Boolean getStopword() {
			return stopword;
		}



		public void setStopword(Boolean stopword) {
			this.stopword = stopword;
		}



		public void setPosTagClass(String posTagClass) {
			this.posTagClass = posTagClass;
		}



		public void setProb(double prob) {
	        this.prob = prob;
	    }

	    public String getSecTag() {
	        return secTag;
	    }

	    public void setSecTag(String secTag) {
	        this.secTag = secTag;
	    }

	    public double getSecTagProb() {
	        return secTagProb;
	    }

	    public void setSecTagProb(double secTagProb) {
	        this.secTagProb = secTagProb;
	    }
	    
	       
	    public int getPosition() {
	        return position;
	    }

	    public void setPosition(int position) {
	        this.position = position;
	    }

	    public String getPosTag() {
	        return posTag;
	    }

	    public void setPosTag(String posTag) {
	        this.posTag = posTag;
	    }

	    public String getLemma() {
	        return lemma;
	    }

	    public void setLemma(String lemma) {
	        this.lemma = lemma;
	    }

	    public String getWord() {
	        return word;
	    }

	    public void setWord(String word) {
	        this.word = word;
	    }
	    
	    public void generateTagClass() {
	    	if (posTag.toLowerCase().startsWith("n")) {
				posTagClass="Nomina";
			} else if (posTag.toLowerCase().startsWith("adj")) {
				posTagClass="Adjektiv";
			} else if (posTag.toLowerCase().startsWith("card")) {
				posTagClass="Zahl";
			} else if (posTag.toLowerCase().startsWith("v")) {
				posTagClass="Verb";
			} else if (posTag.toLowerCase().startsWith("art")) {
				posTagClass="Artikel";
			} else if (posTag.toLowerCase().startsWith("adv")) {
				posTagClass="Adverb";
			} else if (posTag.toLowerCase().startsWith("ko")) {
				posTagClass="Konjunktion";
			} else if (posTag.toLowerCase().startsWith("ap")) {
				posTagClass="Adposition";
			} else if (posTag.toLowerCase().startsWith("pt")) {
				posTagClass="Partikel";
			} else if (posTag.toLowerCase().startsWith("itj")) {
				posTagClass="Interjektion";
			} else if (posTag.toLowerCase().startsWith("trunc")) {
				posTagClass="Kompositions-Erstglied";
			} else if (posTag.toLowerCase().startsWith("xy")) {
				posTagClass="Nichtwort";
			} else if (posTag.toLowerCase().startsWith("fm")) {
				posTagClass="fremdsprachliches Material";
			} else if (posTag.toLowerCase().startsWith("p")) {
				posTagClass="Pronomen";
			} else if (posTag.toLowerCase().startsWith("$")) {
				posTagClass="Satzzeichen";
			} else {
				posTagClass=posTag;
			}
	    }

	    
	    
		

		@Override
		public String toString() {
			return "TaggedWord [word=" + word + ", position=" + position + ", posTag=" + posTag + ", posTagClass="
					+ posTagClass + ", prob=" + prob + ", secTag=" + secTag + ", secTagProb=" + secTagProb + ", lemma="
					+ lemma + ", stopword=" + stopword + "]";
		}



		public String getPosTagClass() {
			// TODO Auto-generated method stub
			return posTagClass;
		}
	    
	    
	}
