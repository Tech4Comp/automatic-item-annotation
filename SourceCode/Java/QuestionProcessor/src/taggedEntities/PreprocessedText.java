package taggedEntities;

import java.util.ArrayList;
import java.util.List;

import statistic.TextStatistic;

public class PreprocessedText {
	private List<PreprocessedSentence> taggedSentences=new ArrayList<PreprocessedSentence>();
	private TextStatistic textStatistic;
	
	public PreprocessedText() {}
	
	public PreprocessedText(List<PreprocessedSentence> taggedSentences) {
		super();
		this.taggedSentences = taggedSentences;
	}	
	
	//public TextStatistic getTextStatistic() {
	//	return textStatistic;
//	}

	public void setTextStatistic(TextStatistic textStatistic) {
		this.textStatistic = textStatistic;
	}

	public List<PreprocessedSentence> getTaggedSentences() {
		return taggedSentences;
	}

	@Override
	public String toString() {
		return "PreprocessedText [taggedSentences=" + taggedSentences + ", textStatistic=" + textStatistic + "]";
	}

}
