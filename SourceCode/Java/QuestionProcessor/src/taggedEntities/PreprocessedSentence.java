package taggedEntities;

import java.util.ArrayList;
import java.util.List;
import statistic.SentenceStatistic;

public class PreprocessedSentence {
	private String sentence;
    private List<TaggedWord>taggedWords=new ArrayList<>();
    private List<SemanticRealationship> semanticRealationships = new ArrayList<SemanticRealationship>();
    private int countWords;
    private SentenceStatistic sentenceStatistic;
    
    // Konstruktoren
	public PreprocessedSentence(String sentence, List<TaggedWord> taggedWords) {
		super();
		this.sentence = sentence;
		this.taggedWords = taggedWords;
		countWords=0;
	}
	public PreprocessedSentence() {}
	
	 public String getSentence() {
	     return sentence;
	 }

	 public void setSentence(String sentence) {
	     this.sentence = sentence;
    }

	 public List<TaggedWord> getTaggedWords() {
	     return taggedWords;
    }

	 public void setTaggedWords(List<TaggedWord> taggedWords) {
	     this.taggedWords = taggedWords;
    }
	 
	 public void addTaggedWord(TaggedWord taggedWord) {
		 taggedWords.add(taggedWord);
		 countWords++;
	 }
	//public int getCountWords() {
	//	return countWords;
//	}
	
	public void clear() {
		sentence="";
		taggedWords.clear();
		countWords=0;
	}
	
	//public List<SemanticRealationship> getSemanticRealationships() {
	//	return semanticRealationships;
//	}
	public void setSemanticRealationships(List<SemanticRealationship> semanticRealationships) {
		this.semanticRealationships = semanticRealationships;
	}
	
	//public SentenceStatistic getSentenceStatistic() {
	//	return sentenceStatistic;
//	}
	public void setSentenceStatistic(SentenceStatistic sentenceStatistic) {
		this.sentenceStatistic = sentenceStatistic;
	}
	public void generateSentenceFromTaggedWords() {
		sentence="";
		for (TaggedWord taggedWord : taggedWords) {
			if (!taggedWord.getWord().isEmpty()) {
				sentence+=taggedWord.getWord()+" ";
			}
			
		}
	}	
	
	@Override
	public String toString() {
		return "TaggedSentence [sentence=" + sentence + ", taggedWords=\n\t" + taggedWords + "\n Semantische Beziehungen=\n\t"+ semanticRealationships +" Satzstatisik "+ sentenceStatistic +"]";
	}     
}
