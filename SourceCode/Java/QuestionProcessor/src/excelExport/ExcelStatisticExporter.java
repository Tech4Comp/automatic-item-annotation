package excelExport;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import helpers.TagDictionary;
import statistic.ListItemStatistic;
import statistic.TextStatistic;
import taggedEntities.SemanticRealationship;

public class ExcelStatisticExporter {
	Workbook workbook= new XSSFWorkbook();
	String outDir;
	public ExcelStatisticExporter(String dir) {
		outDir=dir;
	}
	
	public void createExcelFromTextStatisticList(List<String> tagger, List<TextStatistic> textStatistics) {
		Sheet sheet= workbook.createSheet("Überblick");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		
		// Überblicksseite erstellen
		Row headerRow = sheet.createRow(0);	
		Cell cell = headerRow.createCell(0);
		for (int i = 0; i < tagger.size(); i++) {
			  cell = headerRow.createCell(i+1);
			  cell.setCellValue(tagger.get(i));
			  cell.setCellStyle(headerCellStyle);
			}
		int rowNum = 1;
		
		Row row1 = sheet.createRow(rowNum++);
		Row row2 = sheet.createRow(rowNum++);
		Row row3 = sheet.createRow(rowNum++);
		row1.createCell(0).setCellValue("Token pro Satz");
		row2.createCell(0).setCellValue("Anzahl Sätze");
		row3.createCell(0).setCellValue("Anzahl Token");
		int j=1;
		for (TextStatistic textStatistic : textStatistics) {
			row1.createCell(j).setCellValue(textStatistic.getTokenPerSentence());
			row2.createCell(j).setCellValue(textStatistic.getCountSentence());
			row3.createCell(j).setCellValue(textStatistic.getCountToken());
			j++;
		}	
		for (int i = 0; i < tagger.size(); i++) {
			  sheet.autoSizeColumn(i);
		}
		
		// Worksheet mit den Tagklassen
		sheet= workbook.createSheet("Tagklassen");
		headerRow = sheet.createRow(0);
				
		// Header hinzufügen
		for (int i = 0; i < tagger.size(); i++) {
			cell = headerRow.createCell(i+1);
			cell.setCellValue(tagger.get(i));
			cell.setCellStyle(headerCellStyle);
					}
			rowNum = 1;
				
			for (Map.Entry<String, String> sttsClass : helpers.TagDictionary.sttsTagClasses.entrySet()) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(sttsClass.getValue());
				j=1;
				for (TextStatistic textStatistic : textStatistics) {
					if (textStatistic.getPosClass().containsKey(sttsClass.getValue())) {
						row.createCell(j).setCellValue(textStatistic.getPosClass().get(sttsClass.getValue()).intValue());
						} else {
							row.createCell(j).setCellValue(0);
					}
						j++;
				}			
			}				
			for (int i = 0; i < tagger.size(); i++) {
				  sheet.autoSizeColumn(i);
			}
			
			// Worksheet mit den Tags
			sheet= workbook.createSheet("Tags");
			headerRow = sheet.createRow(0);
			// Header hinzufügen
			for (int i = 0; i < tagger.size(); i++) {
				  cell = headerRow.createCell(i+1);
				  cell.setCellValue(tagger.get(i));
				  cell.setCellStyle(headerCellStyle);
				}
			rowNum = 1;
			for (Map.Entry<String, String> tag : helpers.TagDictionary.sttsTags.entrySet()) {
				j=1;
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(tag.getValue());
				for (TextStatistic textStatistic : textStatistics) {
					if (textStatistic.getPos().containsKey(tag.getKey())) {
						row.createCell(j).setCellValue(textStatistic.getPos().get(tag.getKey()).intValue());
					} else {
						row.createCell(j).setCellValue(0);
					}		
					j++;
				}		
			}
			for (int i = 0; i < tagger.size(); i++) {
				sheet.autoSizeColumn(i);
				}
			
			// Worksheet mit den Lemmas
			sheet= workbook.createSheet("Lemma");
			headerRow = sheet.createRow(0);
			// Header hinzufügen
			for (int i = 0; i < tagger.size(); i++) {
				  cell = headerRow.createCell(i+1);
				  cell.setCellValue(tagger.get(i));
				  cell.setCellStyle(headerCellStyle);
				}
			rowNum = 1;		
			j=1;
			Map<String, Integer> allreadyProceed= new HashMap<String, Integer>();
			for (TextStatistic textStatistic : textStatistics) {		
				for (Map.Entry<String, Integer> tag : textStatistic.getLemma().entrySet()) {
					if (!allreadyProceed.containsKey(tag.getKey())) {
						allreadyProceed.put(tag.getKey(), rowNum);
						Row row = sheet.createRow(rowNum++);
						row.createCell(0).setCellValue(tag.getKey());
						row.createCell(j).setCellValue(tag.getValue());
						for(int k=j-1; k>=1;k--) {						
							row.createCell(k).setCellValue(0);
						}
						for(int k=j+1; k<=3;k++) {						
							row.createCell(k).setCellValue(0);
						}
					} else {
						sheet.getRow(allreadyProceed.get(tag.getKey())).createCell(j).setCellValue(tag.getValue());					
					}						
				}
				j++;
			}
			for (int i = 0; i < tagger.size(); i++) {
				sheet.autoSizeColumn(i);
				}
			//Worksheet mit Semantischen Beziehungen
			if (tagger.contains("Stanford")) {
				sheet= workbook.createSheet("Semmantische Beziehungen");
				headerRow = sheet.createRow(0);
				
				// Header hinzufügen
				cell = headerRow.createCell(0);
				cell.setCellValue("Quelle");
				cell.setCellStyle(headerCellStyle);
				cell = headerRow.createCell(1);
				cell.setCellValue("Beziehung");
				cell.setCellStyle(headerCellStyle);
				cell = headerRow.createCell(2);
				cell.setCellValue("Ziel");
				cell.setCellStyle(headerCellStyle);
				cell = headerRow.createCell(3);
				cell.setCellValue("Anzahl");
				cell.setCellStyle(headerCellStyle);
				
				rowNum = 1;				
				int indexStanford= tagger.indexOf("Stanford");
				Map<SemanticRealationship, Integer> semmanticRelationships=textStatistics.get(indexStanford).getSemanticRelationships();
				for (Map.Entry<SemanticRealationship, Integer> tag : semmanticRelationships.entrySet()) {
					Row row = sheet.createRow(rowNum++);
					row.createCell(0).setCellValue(tag.getKey().getSource());
					String relationTag= tag.getKey().getRelation();
					if (TagDictionary.semanticDependencyTags.containsKey(relationTag)) {
						relationTag= TagDictionary.semanticDependencyTags.get(relationTag);
					}
					row.createCell(1).setCellValue(relationTag);
					row.createCell(2).setCellValue(tag.getKey().getDestination());
					row.createCell(3).setCellValue(tag.getValue());
				}
				
				for (int i = 0; i < 5; i++) {
					sheet.autoSizeColumn(i);
					}
			}
			//Speichern
			FileOutputStream fileOut;
			try {
				fileOut = new FileOutputStream(outDir+"\\ItemStatistic.xlsx");
				try {
					workbook.write(fileOut);
					fileOut.close();
					workbook.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
	
	public void createExcelFromListItemStatistic(List<String> tagger,List<ListItemStatistic> listItemStatistics) {
		// Test Erstellen einer Excel-Datei
		
		workbook= new XSSFWorkbook();
		
		// Überblicks Worksheet
		Sheet sheet= workbook.createSheet("Überblick");
		Font headerFont = workbook.createFont();
		headerFont.setBold(true);
		CellStyle headerCellStyle = workbook.createCellStyle();
		headerCellStyle.setFont(headerFont);
		
		Row headerRow = sheet.createRow(0);	
		Cell cell = headerRow.createCell(0);
		for (int i = 0; i < tagger.size(); i++) {
			  cell = headerRow.createCell(i+1);
			  cell.setCellValue(tagger.get(i));
			  cell.setCellStyle(headerCellStyle);
			}
		int rowNum = 1;
		
		Row row1 = sheet.createRow(rowNum++);
		Row row2 = sheet.createRow(rowNum++);
		Row row3 = sheet.createRow(rowNum++);
		row1.createCell(0).setCellValue("Token pro Satz");
		row2.createCell(0).setCellValue("Anzahl Sätze");
		row3.createCell(0).setCellValue("Anzahl Token");
		int j=1;
		for (ListItemStatistic listItemStatistic : listItemStatistics) {
			row1.createCell(j).setCellValue(listItemStatistic.getTokenPerSentence());
			row2.createCell(j).setCellValue(listItemStatistic.getCountSentence());
			row3.createCell(j).setCellValue(listItemStatistic.getCountToken());
			j++;
		}	
		for (int i = 0; i < tagger.size(); i++) {
			  sheet.autoSizeColumn(i);
			}
		
		// Worksheet mit den Tagklassen
		sheet= workbook.createSheet("Tagklassen");
		headerRow = sheet.createRow(0);
		
		// Header hinzufügen
		for (int i = 0; i < tagger.size(); i++) {
			  cell = headerRow.createCell(i+1);
			  cell.setCellValue(tagger.get(i));
			  cell.setCellStyle(headerCellStyle);
			}
		rowNum = 1;

		
		for (Map.Entry<String, String> sttsClass : helpers.TagDictionary.sttsTagClasses.entrySet()) {
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(sttsClass.getValue());
			j=1;
			for (ListItemStatistic listItemStatistic : listItemStatistics) {
				if (listItemStatistic.getPosClasses().containsKey(sttsClass.getValue())) {
					row.createCell(j).setCellValue(listItemStatistic.getPosClasses().get(sttsClass.getValue()).intValue());
				} else {
					row.createCell(j).setCellValue(0);
				}
				j++;
			}			
		}				
		for (int i = 0; i < tagger.size(); i++) {
			  sheet.autoSizeColumn(i);
		}
		
		// Worksheet mit den Tags
		sheet= workbook.createSheet("Tags");
		headerRow = sheet.createRow(0);
		// Header hinzufügen
		for (int i = 0; i < tagger.size(); i++) {
			  cell = headerRow.createCell(i+1);
			  cell.setCellValue(tagger.get(i));
			  cell.setCellStyle(headerCellStyle);
			}
		rowNum = 1;
		for (Map.Entry<String, String> tag : helpers.TagDictionary.sttsTags.entrySet()) {
			j=1;
			Row row = sheet.createRow(rowNum++);
			row.createCell(0).setCellValue(tag.getValue());
			for (ListItemStatistic listItemStatistic : listItemStatistics) {
				if (listItemStatistic.getPos().containsKey(tag.getKey())) {
					row.createCell(j).setCellValue(listItemStatistic.getPos().get(tag.getKey()).intValue());
				} else {
					row.createCell(j).setCellValue(0);
				}		
				j++;
			}
			
		}
		for (int i = 0; i < tagger.size(); i++) {
			sheet.autoSizeColumn(i);
				}
		
		// Worksheet mit den Lemmas
		sheet= workbook.createSheet("Lemma");
		headerRow = sheet.createRow(0);
		// Header hinzufügen
		for (int i = 0; i < tagger.size(); i++) {
			  cell = headerRow.createCell(i+1);
			  cell.setCellValue(tagger.get(i));
			  cell.setCellStyle(headerCellStyle);
			}
		rowNum = 1;		
		j=1;
		Map<String, Integer> allreadyProceed= new HashMap<String, Integer>();
		for (ListItemStatistic listItemStatistic : listItemStatistics) {		
			for (Map.Entry<String, Integer> tag : listItemStatistic.getLemma().entrySet()) {
				if (!allreadyProceed.containsKey(tag.getKey())) {
					allreadyProceed.put(tag.getKey(), rowNum);
					Row row = sheet.createRow(rowNum++);
					row.createCell(0).setCellValue(tag.getKey());
					row.createCell(j).setCellValue(tag.getValue());
					for(int k=j-1; k>=1;k--) {						
						row.createCell(k).setCellValue(0);
					}
					for(int k=j+1; k<=3;k++) {						
						row.createCell(k).setCellValue(0);
					}
				} else {
					sheet.getRow(allreadyProceed.get(tag.getKey())).createCell(j).setCellValue(tag.getValue());					
				}						
			}
			j++;
		}
		for (int i = 0; i < tagger.size(); i++) {
			sheet.autoSizeColumn(i);
			}
		//Worksheet mit Semantischen Beziehungen
		if (tagger.contains("Stanford")) {
			sheet= workbook.createSheet("Semmantische Beziehungen");
			headerRow = sheet.createRow(0);
			
			// Header hinzufügen
			cell = headerRow.createCell(0);
			cell.setCellValue("Quelle");
			cell.setCellStyle(headerCellStyle);
			cell = headerRow.createCell(1);
			cell.setCellValue("Beziehung");
			cell.setCellStyle(headerCellStyle);
			cell = headerRow.createCell(2);
			cell.setCellValue("Ziel");
			cell.setCellStyle(headerCellStyle);
			cell = headerRow.createCell(3);
			cell.setCellValue("Anzahl");
			cell.setCellStyle(headerCellStyle);
			
			rowNum = 1;				
			int indexStanford= tagger.indexOf("Stanford");
			Map<SemanticRealationship, Integer> semmanticRelationships=listItemStatistics.get(indexStanford).getSemanticRelationships();
			for (Map.Entry<SemanticRealationship, Integer> tag : semmanticRelationships.entrySet()) {
				Row row = sheet.createRow(rowNum++);
				row.createCell(0).setCellValue(tag.getKey().getSource());
				String relationTag= tag.getKey().getRelation();
				if (TagDictionary.semanticDependencyTags.containsKey(relationTag)) {
					relationTag= TagDictionary.semanticDependencyTags.get(relationTag);
				}
				row.createCell(1).setCellValue(relationTag);
				row.createCell(2).setCellValue(tag.getKey().getDestination());
				row.createCell(3).setCellValue(tag.getValue());
			}
			
			for (int i = 0; i < 5; i++) {
				sheet.autoSizeColumn(i);
				}
		}		
		//Speichern
		FileOutputStream fileOut;
		try {
			fileOut = new FileOutputStream(outDir+"\\ItemStatistic.xlsx");
			try {
				workbook.write(fileOut);
				fileOut.close();
				workbook.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	
}
