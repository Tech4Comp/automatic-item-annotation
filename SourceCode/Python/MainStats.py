# User definied classes
from TextFeatureFactory import *
from PreprocessedItemFactory import *
from CsvImporter import *
from Item import *
from ConfigReader import *
from Result import *
from mysqlExporter import *
from jsonImporter import *
from StatsExporter import StatsExporter

# Config einlesen
configReader=ConfigReader("config.xml")
config=configReader.readConfig()
preprocessingCombinations=config.getPreprocessing()
featureCombinations=config.getFeatures()
dataMiningMethodes=config.getDMMethods()
classificationTarget=config.getClassificationTarget()
itemData=config.getItemSets()
dbData=config.getDbData()

# Logger 
logfile=open("log.log","w")

# Initialize global Variables
taggedItemFactory=PreprocessedItemFactory()
textFeatureFactory=TextFeatureFactory()
jsonImporter=JsonImporter()
results=[]

## Methods for processing
def generateItemsFromCsvRaw(path):
    items=[]
    # CSV importieren
    csvImporter=CsvImporter(inFile=path)
    csvImporter.readFile()
    # create Items from CSV import
    for data in csvImporter.dataSet:
        data=csvImporter.dataSet[data]
        # Create Item Object
        item=Item.Item(question=data.get("question",""), description=data.get("description",""), id=data.get("id",""),\
            itemType=data.get("type",""), points=data.get("points",""), level=data.get("level","0"), minnumber=data.get("minnumber",0), maxnumber=data.get("maxnumber",0))
        # Add to Items Array
        items.append(item)
    return items

def generatePreprocessedItemsFromItems(items):
    taggedItems=[]
    for item in items:
        taggedItem=taggedItemFactory.tagItem(item)
        taggedItems.append(taggedItem)
    return taggedItems

## processing according to config
run=1
countItemset=1
exporter= StatsExporter(".\\ressources","stats2")
# Importing Itemsets: Possible are raw data in csv and preprocessedItems as json
for itemset in config.getItemSets():
    logfile.write("Start Itemset "+ str(countItemset) +" of "+str(len(config.getItemSets()))+ " " +str(itemset)+"\n")
    preprocessedItems=[]
    if itemset.get("status")=="raw":
         if itemset.get("format")=="csv":
             items=generateItemsFromCsvRaw(itemset.get("path"))
             preprocessedItems=generatePreprocessedItemsFromItems(items)
    elif itemset.get("status")=="preprocessedItems":
        if itemset.get("format")=="json":
            preprocessedItems=jsonImporter.importJson(itemset.get("path"), "preprocessedItems")
    else:
        raise NameError("Not Found")
    # Print a stats sheet for each itemset
    exporter.printItemStats(sheetname=itemset.get("id"), preprocessedItems=preprocessedItems)
exporter.close()  

logfile.close()
