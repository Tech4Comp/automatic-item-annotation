from pattern.de import parse, split, tag, pprint, lemma, tokenize, parsetree
from TagDictionary import *
from Item import *


class PreprocessedTextFactory():
    def __init__(self, stopwordPath=""):
        self.name=""
        self.stopwordPath=stopwordPath       
        file=open(self.stopwordPath, "r", encoding="utf8")
        text=file.read()
        self.stopwords=text.split(",")

    def proceedText(self, text):
        # Text in Sätze teilen
        sentences=self.sentenize(text)
        preprocessedSentences=[]
        nouns=[]
        verbs=[]
        # Die einzelnen Sätze des Textes taggen
        for sentence in sentences:
            processedSentence=self.proceedSentence(sentence)
            preprocessedSentences.append(processedSentence)
        return(PreprocessedText(preprocessedSentence=preprocessedSentences, nouns=nouns, verbs=verbs))

    def sentenize(self, text):
        output=tokenize(text, punctuation=".,;:!?()[]{}`''\"@#$^&*+-|=~_", replace={})
        return output

    def proceedSentence(self, sentence):
        # Tokens im Satz extrahieren und taggen
        tokens=tag(sentence, tagset='STTS')
        taggedWords=[]
        i=0
        for word, posTag in tokens:
            i+=1
            wordLemma=lemma(word)
            posTagClass=""
            # Tag Klasse bestimmen
            for tagStart, tagClass in SttsTagClasses.sttsTagClasses.items():
                if posTag.startswith(tagStart):
                    posTagClass=tagClass
            isStopword=False
            if word in self.stopwords:
                isStopword=True
            taggedWords.append(TaggedWord(word, i, posTag, posTagClass, wordLemma, isStopword))
        return(PreprocessedSentence(sentence=sentence, taggedWords=taggedWords))

    def getWordsByClass(self, taggedWords, wordClass):
        output=[]
        for word in taggedWords:
            if word.posTagClass==wordClass:
                output.append(word)
        return output
