from os import path
import xlsxwriter

class statisticExporter():
    def __init__(self, directory="", filename=""):
        self.directory=directory
        self.filename=filename
        self.fullPath=directory+"\\"+filename
        if ".xlsx" not in filename:
            self.fullPath+=".xlsx"
        
        exists=path.isfile(self.fullPath)
        self.workbook=xlsxwriter.Workbook(self.fullPath)

    def close(self):
        self.workbook.close()
            
            
    
    def printSciKitStatistic(self, worksheetname="", matrix=None, stats=None):       
        worksheet=self.workbook.add_worksheet(worksheetname)
        # Matrix abbilden
        size=len(matrix)
        row=0
        column=1
        while column<=size:
            worksheet.write(row, column, column)
            column+=1
        column=0
        for line in matrix:
            row+=1
            worksheet.write(row, column, row)
            column+=1
            for field in line:
                worksheet.write(row, column, field)
                column+=1
            column=0
        #Stats
        if len(stats)>0:
            row+=3
            headerRow=(row-1)
            i=0
            for stat in stats:
                worksheet.write(row, column, stat)
                column+=1
                sum=0.0
                count=0
                for value in stats[stat]:
                    worksheet.write(row, column, value)
                    count+=1
                    sum+=value
                    column+=1
                # Im ersten Durchlauf Überschriften erstellen
                if i==0:
                    headerColumn=0
                    worksheet.write(headerRow, headerColumn, "Wert")
                    i+=1
                    while i<=count:
                        headerColumn+=1
                        worksheet.write(headerRow, headerColumn, str(i))
                        i+=1
                    worksheet.write(headerRow, headerColumn+1, "Mittelwert")
                worksheet.write(row, column, sum/count)
                column=0
                row+=1
                i+=1    