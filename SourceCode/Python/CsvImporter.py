class CsvImporter():
    def __init__(self, inFile="", rowDelimiter="$$$", cellDelimiter="$" ):
        self.text=""
        self.inFile=inFile
        self.rowDelimiter=rowDelimiter
        self.cellDelimiter=cellDelimiter
        self.dataSet={}
        

    def readFile(self):
        file=open(self.inFile,"r",encoding="utf8")
        plainText=file.read()
        self.text=self.cleanString(plainText)
        self.dataSet=self.processString(self.text)
        

    
    def cleanString(self, plainText):
        cleanedText=plainText
        # HTML Zeichen
        cleanedText=cleanedText.replace("&nbsp;"," ")
        cleanedText=cleanedText.replace("&amp;"," ")
        cleanedText=cleanedText.replace("&gt;"," ")
        cleanedText=cleanedText.replace("\n"," ")
        # Wiederholende Phrasen ohne Aussagekraft streichen
        cleanedText=cleanedText.replace("Wählen Sie mindestens eine, maximal vier aus den fünf Antwortoptionen aus.","")
        cleanedText=cleanedText.replace("Punkte erhalten Sie für jede richtige Antwort (Teilpunktbewertung)","")
        cleanedText=cleanedText.replace("Wählen Sie eine aus den vier Antwortoptionen aus.","")

        # Satzzeichen normalisiseren
        cleanedText=cleanedText.replace(" .",".")
        cleanedText=cleanedText.replace("?","? ")

        return cleanedText

    def processString(self, inText):
        rows=inText.split(self.rowDelimiter)
        header=[]
        dataset={}
        if len(rows)>0:
            headerCells=rows[0].split(self.cellDelimiter)
            for cell in headerCells:
                header.append(cell)
            rows.pop(0)
        for row in rows:
            cells=row.split(self.cellDelimiter)
            data={}
            i=0
            for cell in cells:
                if  cell is None:
                    cell=""
                data[header[i]]=cell
                i+=1
                #print(cell)
            print(data)
            if data["id"]:
                dataset[data["id"]]=data
        return dataset
