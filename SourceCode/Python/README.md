# Redme
Used version is python 3.6
Get pattern from here: https://github.com/clips/pattern
Install sklearn, pandas, numpy, mysql-connector-python, xlsxwriter, xlrd, json

You will only need the following files:
## Main.py

Process Items according to config

## MainStats.py
Generate an Excel Sheet with different Stats about the preprocessed items

## knowledgebsaedsystem.py
Use the knowledge based annotation. Rules are stored in ressouces/rules.xlsx

## Config 
Main and MainStats use a config file:
config.xml (description inside)
