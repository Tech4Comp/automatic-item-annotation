import xml.etree.ElementTree as ET
from Config import *

class ConfigReader():
    def __init__(self, pathToConfig):
        self.tree=ET.parse(pathToConfig)

    def readConfig(self):
        root=self.tree.getroot()
        textprocessingOptions={}
        features=[]
        forbiddenCombinations=[]
        algorithms=[]
        classificationTarget=""
        dimensionality=6
        for configGroup in root:
            if configGroup.tag=="itemsets":
                itemSets=[]
                for itemset in configGroup:
                    itemData={}
                    itemData["format"]=itemset.find("format").text              
                    itemData["path"]=itemset.find("path").text
                    itemData["status"]=itemset.find("status").text
                    itemData["id"]=itemset.find("id").text
                    itemData["testbed"]=itemset.find("testbed").text
                    dimensionality=itemset.find("dimensionality").text
                    itemSets.append(itemData)
            if configGroup.tag=="database":
                dbData={}
                dbData["host"]=configGroup.find("host").text
                dbData["user"]=configGroup.find("user").text
                dbData["password"]=configGroup.find("password").text
                dbData["database"]=configGroup.find("database").text
                clears=[]
                for itemset in configGroup.find("clearItemsets"):
                    clears.append(itemset.text)
                dbData["clearItemsets"]=clears
            if configGroup.tag=="classificationTarget":
                classificationTarget=configGroup.text
            if configGroup.tag=="textprocessing":
                for option in configGroup:
                    if option.tag=="stemps":
                        textprocessingOptions["stemps"]=option.text
                    if option.tag=="stopwords":
                        textprocessingOptions["stopwords"]=option.text
                    
            if configGroup.tag=="featureSelection":
                for option in configGroup:
                    if option.tag=="features":
                        for attribute in option:
                            attr={}
                            attr["name"]=attribute.find("name").text
                            attr["type"]=attribute.find("type").text
                            attr["relative"]=attribute.find("relative").text
                            attr["category"]=attribute.find("category").text
                            features.append(attr)
                    if option.tag=="forbiddenCombinations":
                        for combination in option:
                            combo=[]
                            for attr in combination:
                                combo.append(attr.text)
                            forbiddenCombinations.append(combo)
            if configGroup.tag=="miningAlgorithms":
                for option in configGroup:
                    if option.tag=="algorithm":
                        algorithms.append(option.find("name").text)

        config=Config(classificationTarget=classificationTarget, textprocessingOptions=textprocessingOptions, features=features, 
            forbiddenCombinations=forbiddenCombinations, algorithms=algorithms, dbConnection=dbData, itemSets=itemSets,dimensionality=dimensionality)
        return(config)