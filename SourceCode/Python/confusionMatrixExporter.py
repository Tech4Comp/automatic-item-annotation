class ConfusionExporter():
    def __init__(self, directory=""):
        self.directory=directory
    
    def exportMatrix(self, name="", matrix=None):
        outstring=";"
        size=len(matrix)
        i=1
        while i<=size:
            outstring+=str(i)+";"
            i+=1
        outstring=outstring[:-1]
        outstring+="\n"
        i=1
        while i<=size:
            outstring+=str(i)+";"
            j=1
            while j<=size:
                 outstring+=str(matrix[str(i)].get(str(j)))+";"
                 j+=1
            outstring=outstring[:-1]
            outstring+="\n"
            i+=1
        outFile=open(self.directory+"\\"+name+".csv","w", encoding="utf8")
        outFile.write(outstring)
        outFile.close()
    
    def exportSciMatrix(self, name="", matrix=None):
        outstring=";"
        size=len(matrix)
        i=1
        while i<=size:
            outstring+=str(i)+";"
            i+=1
        outstring=outstring[:-1]
        outstring+="\n"
        i=1
        for line in matrix:
            outstring+=str(i)+";"
            for field in line:
                outstring+=str(field)+";"
            outstring=outstring[:-1]
            outstring+="\n"
            i+=1
        outFile=open(self.directory+"\\"+name+".csv","w", encoding="utf8")
        outFile.write(outstring)
        outFile.close()
