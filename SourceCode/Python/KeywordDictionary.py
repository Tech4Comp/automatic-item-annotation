class KeywordDirctionary():
    keywords={
        "keyword":"keywordmatches",
        "aus,":"from",
        "aufgabe":"featuredItem",
        "in":"in",
        "itemtyp":"itemType",
        "ist":"=",
        "singel-choice":"sc",
        "multiple-choice":"mc",
        "freitext":"ft",
        "zuordnung":"zu",
        "markierung":"ma",
        "ja/nein":"jn",
        "gewicht":"weight",
        "größten":"max",
        "erhöhe":"+=",
        "und":" and ",
        "anforderungsstufe":"level",
        "wissen":"knowledge",
        "verstehen":"understanding",
        "anwenden":"application",
        "analyse":"analyse",
        "synthese":"create",
        "evaluation":"evaluate",
        "knowledge":"wissen",
        "understanding":"verstehen",
        "application":"anwenden",
        "analyse":"analyse",
        "create":"synthese",
        "evaluate":"evaluation"
    }

class PerformanceLeveToInt():
    keywords={
        "wissen":1,
        "verstehen":2,
        "anwenden":3,
        "analyse":4,
        "synthese":5,
        "evaluation":6
    }