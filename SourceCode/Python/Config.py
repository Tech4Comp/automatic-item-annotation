import itertools

class Config():
    def __init__(self, classificationTarget="", textprocessingOptions=[], features=[], 
    forbiddenCombinations=[], algorithms=[], dbConnection={}, itemSets=[],dimensionality=6):
        self.textprocessingOptions=textprocessingOptions
        self.features=features
        self.forbiddenCombinations=forbiddenCombinations
        self.algorithms=algorithms
        self.classificationTarget=classificationTarget
        self.dbConnection=dbConnection
        self.itemSets=itemSets
        self.dimensionality=dimensionality

    def getPreprocessing(self):
        combinations=[]
        stopwords=[]
        stemps=[]
        print(self.textprocessingOptions)
        for key, value in self.textprocessingOptions.items():
            if(value=="test"):
                if key=="stemps":
                    stemps.append(True)
                    stemps.append(False)
                else:
                    stopwords.append(True)
                    stopwords.append(False)
            else:
                if key=="stemps":
                    stemps.append(value)
                else:
                    stopwords.append(value)
        for stemp in stemps:
            option={}
            option["stemps"]=stemp
            for stopword in stopwords:
                option["stopwords"]=stopword
                combinations.append(option)
                stemp=stemp
                option={}
                option["stemps"]=stemp
        return combinations

    def getFeatures(self):
        combinations=[]
        for L in range(0, len(self.features)+1):
            for subset in itertools.combinations(self.features, L):
                if (len(subset)>0):
                    forbidden=False
                    attributes=[]
                    for subs in subset:
                        attributes.append(subs.get("name"))
                    for forbiddenCombination in self.forbiddenCombinations:
                        #print(forbiddenCombination)
                       # print(attributes)
                        #print(all(b in attributes for b in forbiddenCombination))
                        if all(b in attributes for b in forbiddenCombination):
                            forbidden=True
                    if not forbidden:
                        if len(subset)<=4:
                            combinations.append(subset)
        print(len(combinations))
        return combinations
    def getFeatureList(self):
        return self.features
    def getDMMethods(self):
        return self.algorithms

    def getClassificationTarget(self):
        return self.classificationTarget
    
    def getDbData(self):
        return self.dbConnection

    def getItemSets(self):
        return self.itemSets

    def getTestbed(self):
        return self.test

    def __str__(self):
        return ("Textprocessing: "+str(self.textprocessingOptions) +"\n"+
            "Features: "+ str(self.features)+"\n"+"forbiddenCombinations: "+ str(self.forbiddenCombinations) + "\n"
                "Algorithms:" +str(self.algorithms)
        )
        