# User definied classes
from TextFeatureFactory import *
from PreprocessedItemFactory import *
from CsvImporter import *
from Item import *
from ConfigReader import *
from Result import *
from MysqlExporter import *
from JsonImporter import *


# Turn off scikit warnings
def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn

# sklearn classes
## Feature extracation and mapping
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction import DictVectorizer
from sklearn_pandas import DataFrameMapper
from sklearn import preprocessing

## Validation
from sklearn.model_selection import train_test_split, cross_val_score, cross_validate, KFold,RepeatedKFold, LeaveOneOut
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

## Classifier
from sklearn.neural_network import MLPClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis

# Standard libaries 
import pandas as pd
import numpy as np
import time
import statistics
# Helper - Calculate messures from confusion matrix
def confMatrixToReport(confMatrix):
    #print(confMatrix)
    scores={}
    scores["resultsets"]=[]
    size=len(confMatrix)
    level=1
    samplesize=0
    correct=0
    for line in confMatrix:
        for field in line:
            samplesize+=field
    # Add True Positive
    while level<=size:
        score={}
        score["tp"]=confMatrix[level-1][level-1]
        correct+=score["tp"]
        score["fp"]=0
        score["fn"]=0
        score["tn"]=0
        score["support"]=0
        for i, line in enumerate(confMatrix, start=1):
            if i==level:
                for j, number in enumerate(line, start=1):
                    
                    #print(index)
                    if j!=level:
                        score["fp"]+=number
            else:         
                score["fn"]+=line[level-1]
                for j, number in enumerate(line, start=1):
                    #print(index)
                    if j!=level:
                        score["tn"]+=number
        # calculate precision and recall       
        if  score["tp"]!=0:      
            score["precision"]=score["tp"]/(score["tp"]+score["fp"])
            score["recall"]=score["tp"]/(score["tp"]+score["fn"])
        else:
            score["precision"]=0
            score["recall"]=0
        
        if score["tp"]!=0 or score["tn"]!=0:
            score["accuracy"]=(score["tp"]+score["tn"])/(score["tp"]+score["fp"]+score["tn"]+score["fn"])
        else:
            score["accuracy"]=0

        if score["precision"]!=0 or score["recall"]!=0:
            score["f1"]=2*((score["precision"]*score["recall"])/(score["precision"]+score["recall"]))
        else:
            score["f1"]=0
        score["level"]=level
        scores["resultsets"].append(score)
        level+=1
    scores["accuracy"]=correct/samplesize
    return scores


# Config einlesen
configReader=ConfigReader("config.xml")
config=configReader.readConfig()
preprocessingCombinations=config.getPreprocessing()
featureCombinations=config.getFeatures()
dataMiningMethodes=config.getDMMethods()
classificationTarget=config.getClassificationTarget()
itemData=config.getItemSets()
dbData=config.getDbData()

# Logger 
logfile=open("log.log","w")

# Initialize global Variables
taggedItemFactory=PreprocessedItemFactory()
textFeatureFactory=TextFeatureFactory()
jsonImporter=JsonImporter()
results=[]

# Initialize Potential Classifier an store them in a dictionary
classifiers={
        # Gauss: Naive Bayes
        "GaussianNB": GaussianNB(),
        # Liniare Support Vector Maschine
        "Linear SVC": SVC(kernel="linear", C=0.025, cache_size=7000),
        "RBF SVC": SVC(gamma=2, C=1, cache_size=7000),
        # k nearest Neighbor
        "KNN": KNeighborsClassifier(3),
        # Entscheidungsbaum
        "DecisionTreeClassifier": DecisionTreeClassifier(max_depth=5),
        # Erzeugt mehrere unkorrelierte Entscheidungsbäume die nach Mehrheitsentscheid 
        # die Klassifikation durchführen
        "RandomForestClassifier": RandomForestClassifier(max_depth=5, n_estimators=10, 
            max_features=1),
        "QuadraticDiscriminantAnalysis": QuadraticDiscriminantAnalysis()
    }

# initialize MYSQL Exporter
exporter=MySqlExporter(host=dbData.get("host"), user=dbData.get("user"), password=dbData.get("password"), 
    database=dbData.get("database"))
exporter.initializeDimensions(config=config)
# delete old data from datebase if wanted
if len(dbData.get("clearItemsets"))>0:
    print(len(dbData.get("clearItemsets")))
    exporter.clearResults(ids=dbData.get("clearItemsets"))

# proceed given itemsets

## Methods for processing
def generateItemsFromCsvRaw(path):
    items=[]
    # CSV importieren
    csvImporter=CsvImporter(inFile=path)
    csvImporter.readFile()
    # create Items from CSV import
    for data in csvImporter.dataSet:
        data=csvImporter.dataSet[data]
        # Create Item Object
        item=Item.Item(question=data.get("question",""), description=data.get("description",""), id=data.get("id",""),\
            itemType=data.get("type",""), points=data.get("points",""), level=data.get("level","0"), minnumber=data.get("minnumber",0), maxnumber=data.get("maxnumber",0))
        # Add to Items Array
        items.append(item)
    return items

def generatePreprocessedItemsFromItems(items):
    taggedItems=[]
    for item in items:
        taggedItem=taggedItemFactory.tagItem(item)
        taggedItems.append(taggedItem)
    return taggedItems

def generateTargetArrayFromFeaturedItems(featuredItems):
    targetClasses=[]
    for featuredItem in featuredItems:
        targetClasses.append(featuredItem.__getattribute__(classificationTarget))
    return targetClasses

def generateFeaturedItemFromPreprossesedItems(preprocessedItems, returnStemps, includeStopwords):
    featuredItems=[]
    for preprocessedItem in preprocessedItems:
            featuredItem=textFeatureFactory.proceedItem(preprocessedItem,
                includeStopwords=True, returnStemps=True) 
                #includeStopwords=includeStopwords, returnStemps=returnStemps)
            featuredItems.append(featuredItem)
    return featuredItems

def generateFeatureVectorFromFeaturedItem(featuredItems, featureCombination):
    featureList={}
    # Feature Liste initialisieren
    for feature in featureCombination:
        featureElement={}
        featureElement["type"]=feature.get("type")
        featureElement["values"]=[]
        name=feature.get("name")
        featureList[name]=featureElement
    # Select needed Features from Item 
    for featuredItem in featuredItems:
        for feature in featureCombination:
            name=feature.get("name")
            value=featuredItem.__getattribute__(name)
            featureList[name]["values"].append(value)

    converter=[]
    pdData={}
    # Convert to an Data Frame conform type 
    for name, feature in featureList.items():
        pdData[name]=feature.get("values")
        # Select preprocessing method depending on the feature type
        if feature.get("type")=="text":
            converter.append((name, CountVectorizer(max_features=1500, min_df=1, max_df=0.7)))
        elif feature.get("type")=="integer":
            converter.append((name, None))   
        elif feature.get("type")=="category":
            converter.append((name, preprocessing.LabelEncoder()))   
        elif feature.get("type")=="dictionary":
            converter.append((name, DictVectorizer()))   
            # Create Vector
    data=pd.DataFrame(pdData)
    mapper = DataFrameMapper(converter)
    featureVector = mapper.fit_transform(data.copy())
    return featureVector

def generateReportForFeatureVector(featureVector, targetClasses, method, run, appId):
    print(method)   
    random_state=12883821   
    # Create a confusion matrix for overall cross validation
    conf_mat = np.zeros((int(config.dimensionality),int(config.dimensionality)))
    targetClasses=[]
    for featuredItem in featuredItems:
        targetClasses.append(featuredItem.__getattribute__(classificationTarget))
    targetClasses=np.asarray(targetClasses)
    fittime=[]
    scoretime=[]

    # get Algortihm from dict
    algorithm=classifiers.get(method)
    # Cross Validation (repeated K-Fold)
    rkf=RepeatedKFold(n_splits=3, n_repeats=10, random_state=random_state)
    for train, test in rkf.split(featureVector):
        # X are features, Y classification targets
        # Split training and testdata
        x_train, x_test = featureVector[train], featureVector[test]
        y_train, y_test = targetClasses[train], targetClasses[test]
        # Model learning
        classifier=algorithm.fit(x_train, y_train)
        # Model Evaluation
        pred=classifier.predict(x_test)
        confusion=confusion_matrix(y_test, pred)
        # Append total confusion matrix
        conf_mat=conf_mat+confusion
    
    stats=confMatrixToReport(conf_mat)
    res= Run(confusionMatrix=conf_mat, runId=run, stats=stats, fittime=fittime, 
        scoretime=scoretime, preprocessingConfig=preprocessingCombination,
    featureCombination=featureCombination, algorithm=method)
    exporter.exportResult(res, appId)
## processing according to config
run=1
countItemset=1
starttime=time.time()
for itemset in config.getItemSets():
    startItemset=time.time()
    logfile.write("Start Itemset "+ str(countItemset) +" of "+str(len(config.getItemSets()))+ " " +str(itemset)+"\n")
    preprocessedItems=[]
    if itemset.get("status")=="raw":
         if itemset.get("format")=="csv":
             items=generateItemsFromCsvRaw(itemset.get("path"))
             preprocessedItems=generatePreprocessedItemsFromItems(items)
    elif itemset.get("status")=="preprocessedItems":
        if itemset.get("format")=="json":
            preprocessedItems=jsonImporter.importJson(itemset.get("path"), "preprocessedItems")
    else:
        raise NameError("Not Found")
    logfile.write("Preprocessing finished after: " + str(time.time()-startItemset)+"\n")
    countPreprocessingCombinations=1
    for preprocessingCombination in preprocessingCombinations:
        startPreprocessing=time.time()
        logfile.write("Start Combination: "+str(countPreprocessingCombinations)+" of "+ str(len(preprocessingCombinations))+" " +str(preprocessingCombination)+"\n")
        print(preprocessingCombination)
        # Feature Generation     
        ## 1 Step: Textpreprocessing (for each combination of given parameters)
        returnStemps=preprocessingCombination.get("stemps")
        includeStopwords=preprocessingCombination.get("stopwords")
        featuredItems=generateFeaturedItemFromPreprossesedItems(preprocessedItems, returnStemps, includeStopwords)
        targetClasses=generateTargetArrayFromFeaturedItems(featuredItems)
        #print(targedClasses)
         # 2 Step: Feature Selection
        countFeatureCombination=1
        for featureCombination in featureCombinations:
            if len(featureCombination)>0:
                logfile.write("Start Featureset: "+ str(countFeatureCombination)+" of " +str(len(featureCombinations))+ " " +str(featureCombination)+"\n")
                startFeature=time.time()
                featureVector=generateFeatureVectorFromFeaturedItem(featuredItems, featureCombination)
                # 3. Step Data Mining
                countMethod=1
                for method in dataMiningMethodes:
                    try:
                        logfile.write("Start Algorithm: "+str(countMethod)+" of "+str(len(dataMiningMethodes))+" "+str(method)+"\n")
                        startMethod=time.time()
                        print(targetClasses)
                        generateReportForFeatureVector(featureVector, targetClasses, method, run, itemset.get("id"))
                        run +=1
                        logfile.write(str(method +" finished afer: "+str(time.time()-startMethod))+"\n")
                        countMethod+=1
                    except:
                        logfile.write("Error using "+str(method))
                        print("Error")

            countFeatureCombination+=1
        logfile.write(str(preprocessingCombination) +" finished afer: "+str(time.time()-startPreprocessing)+"\n")
        countPreprocessingCombinations+=1
    logfile.write(str(itemset) +" finished afer: "+str(time.time()-startItemset)+"\n")
    countItemset+=1
logfile.write("finished afer: "+str(time.time()-starttime)+"\n")
logfile.close()
exporter=ResultExporter()
exporter.writeToFile(results=results)