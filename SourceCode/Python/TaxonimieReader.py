import xml.etree.ElementTree as ET
from Config import *
from Taxonomie import *

class TaxonimieReader():
    def __init__(self, pathToTax):
        self.tree=ET.parse(pathToTax)

    def readTaxonomie(self):
        taxonomie=Taxonomie()
        root=self.tree.getroot()
        print(len(taxonomie.taxonomieLevels))
        for level in root:
            print(level.tag)      
            for propertiy in level:
                keywords=[]
                if propertiy.tag=="keywords":
                    for word in propertiy:
                        keywords.append(word.text)
            taxonomie.addLevelToTaxonomie(TaxonomieLevel(level.tag, keywords))
            print(len(taxonomie.taxonomieLevels))
            #print(str(level.tag)+" "+str(keywords))
        print(len(taxonomie.taxonomieLevels))
        return taxonomie


