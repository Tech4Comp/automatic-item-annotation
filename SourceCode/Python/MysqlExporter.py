import mysql.connector
class MySqlExporter():
    def __init__(self, host="", user="", password="", database=""):
        self.mydb = mysql.connector.connect(
            host=host,
            user=user,
            port="3308",
            passwd=password,
            database=database
        )
        self.createTables()
    
    def createTables(self):
        cursor = self.mydb.cursor()
        fd=open("ressources\initializeDatabase.sql")
        sqlFile=fd.read()
        fd.close()
        sqlCommands=sqlFile.split(";")
        for command in sqlCommands:
            try:
                cursor.execute(command)
            except mysql.connector.Error as e:
                print(e)
    
    def initializeDimensions(self, config=""):
        cursor = self.mydb.cursor()
        for feature in config.getFeatureList():
            statement="""INSERT INTO features (name, type, relative, category) 
            VALUES (%s, %s, %s, %s) ON DUPLICATE KEY UPDATE 
            type=VALUES(type),
            relative=VALUES(relative),
            category=VALUES(category)"""
            values=(feature.get("name"), feature.get("type"), feature.get("relative"), feature.get("category"))
            cursor.execute(statement, values)
            self.mydb.commit()
        
        for algorithm in config.getDMMethods():
            statement="""INSERT IGNORE INTO algorithms VALUES(%s)"""
            cursor.execute(statement, (algorithm,))
            self.mydb.commit()
        
        ppid=0
        for ppCombination in config.getPreprocessing():
            stemps=0
            stopwords=0
            if ppCombination.get("stemps"):
                stemps=1
            if ppCombination.get("stopwords"):
                stopwords=1
            statement="""INSERT IGNORE INTO textprocessingoptions (C_ID, stemps, stopwords) VALUES(%s, %s, %s)"""
            values=(ppid, stemps, stopwords)
            cursor.execute(statement, values)
            self.mydb.commit()
            ppid+=1

    def clearResults(self, ids=[]):
        cursor = self.mydb.cursor()
        for application in ids:
            statement="DELETE FROM runsfeatures WHERE runsfeatures.R_ID IN (SELECT R_ID FROM runs WHERE application=%s)"
            cursor.execute(statement, (application,))
            statement="DELETE FROM runs WHERE application=%s"
            cursor.execute(statement, (application,))
            statement="DELETE FROM resultsets WHERE TRUE"
            cursor.execute(statement)
            self.mydb.commit()

    def exportResult(self, result, appId):
        print("Export started")
        cursor = self.mydb.cursor()
        # Select Preprocessing Option
        statement="SELECT C_ID FROM textprocessingoptions WHERE stopwords=%s AND stemps=%s"
        values=(result.preprocessingConfig.get("stopwords"),result.preprocessingConfig.get("stemps"))
        cursor.execute(statement, values)
        sqlResult = cursor.fetchone()
        cid=sqlResult[0]
        # Insert into results
        statement="""INSERT INTO runs (runId, accuracy, fitTime, scoreTime, confusionMatrix, algorithm, C_ID, application)
        VALUES (%s, %s,%s,%s,%s, %s, %s, %s)"""
        values=(result.runId, float(result.getAccuracy()), float(0), float(0), result.getConfusionMatrix(), result.algorithm, cid, appId)
        #print(values)
        cursor.execute(statement, values)
        lastId=cursor.lastrowid
        for resultset in result.resultSets:
            statement="""INSERT INTO resultsets (level, resultsets.precision, recall, f1, accuracy, R_ID)
                VALUES (%s, %s, %s, %s, %s,%s)"""
            cursor.execute(statement, (resultset["level"], float(resultset["precision"]), float(resultset["recall"]),float(resultset["f1"]),float(resultset["accuracy"]), lastId))
        self.mydb.commit()

        for feature in result.featureCombination:
            #print(feature.get("name"))
            statement="""INSERT INTO runsfeatures (R_ID, name) 
                VALUES ( %s, %s )"""
            cursor.execute(statement, (lastId, feature.get("name")))
        self.mydb.commit()
        return True
