from Item import *
from Taxonomie import *
from TaxonimieReader import *

class TextFeatureFactory():
    def __init__(self):
        self.name=""
        cr=TaxonimieReader("./ressources/taxonomie.xml")
        self.tax=cr.readTaxonomie()

    def proceedItem(self, preprocessedItem, includeStopwords=True, returnStemps=False):
        if isinstance(preprocessedItem, PreprocessedItem):
            # Strukturierte Argumente
            itemType=preprocessedItem.itemType
            id=preprocessedItem.ealidid
            testbed=preprocessedItem.testbed
            level=preprocessedItem.level
            # Strings
            words=preprocessedItem.fullText.getWordsAsString(includeStopwords=includeStopwords, wordClass="All", returnStemps=returnStemps)
            wordsQuestion=preprocessedItem.question.getWordsAsString(includeStopwords=includeStopwords, wordClass="All", returnStemps=returnStemps)
            wordsDescritpion=preprocessedItem.description.getWordsAsString(includeStopwords=includeStopwords, wordClass="All", returnStemps=returnStemps)
            
            # Strings - nur bestimmte Wortklassen
            verbs=preprocessedItem.fullText.getWordsAsString(includeStopwords=includeStopwords, wordClass="Verb", returnStemps=returnStemps)
            nouns=preprocessedItem.fullText.getWordsAsString(includeStopwords=includeStopwords, wordClass="Nomen", returnStemps=returnStemps)
            verbsQuestion=preprocessedItem.question.getWordsAsString(includeStopwords=includeStopwords, wordClass="Verb", returnStemps=returnStemps)
            nounsQuestion=preprocessedItem.question.getWordsAsString(includeStopwords=includeStopwords, wordClass="Nomen", returnStemps=returnStemps)
            verbsDescription=preprocessedItem.description.getWordsAsString(includeStopwords=includeStopwords, wordClass="Verb", returnStemps=returnStemps)
            nounsDescription=preprocessedItem.description.getWordsAsString(includeStopwords=includeStopwords, wordClass="Nomen", returnStemps=returnStemps)

            # Counts - Sentences
            countSentences=len(preprocessedItem.fullText.preprocessedSentences)
            countSentencesQuestion=len(preprocessedItem.question.preprocessedSentences)
            countSentencesDescription=len(preprocessedItem.description.preprocessedSentences)

            # Counts - Token
            countWords=preprocessedItem.fullText.countToken(includeStopwords=includeStopwords, wordClass="All")
            countWordsQuestion=preprocessedItem.question.countToken(includeStopwords=includeStopwords, wordClass="All")
            countWordsDescription=preprocessedItem.description.countToken(includeStopwords=includeStopwords, wordClass="All")

            # Count - Verbs
            countVerbs=preprocessedItem.fullText.countToken(includeStopwords=includeStopwords, wordClass="Verb")
            countVerbsQuestion=preprocessedItem.question.countToken(includeStopwords=includeStopwords, wordClass="Verb")
            countVerbsDescription=preprocessedItem.description.countToken(includeStopwords=includeStopwords, wordClass="Verb")
            # Count - Nouns
            countNouns=preprocessedItem.fullText.countToken(includeStopwords=includeStopwords, wordClass="Nomen")
            countNounsQuestion=preprocessedItem.question.countToken(includeStopwords=includeStopwords, wordClass="Nomen")
            countNounsDescription=preprocessedItem.description.countToken(includeStopwords=includeStopwords, wordClass="Nomen")

            # Verhältnisse
            tokenPerSentence=0.0
            tokenPerSentenceQuestion=0.0
            tokenPerSentenceDescription=0.0

            verbsPerSentence=0.0
            nounsPerSentence=0.0

            verbsPerToken=0.0
            nounsPerToken=0.0

            if countSentences != 0:
                tokenPerSentence=countWords/countSentences
                verbsPerSentence=countVerbs/countSentences
                nounsPerSentence=countNouns/countSentences
            if countSentencesQuestion != 0:
                tokenPerSentenceQuestion=countWordsQuestion/countSentencesQuestion
            if countSentencesDescription !=0:
                tokenPerSentenceDescription=countWordsDescription/countSentencesDescription
            
            if countWords!=0:
                verbsPerToken=countVerbs/countWords
                nounsPerToken=countNouns/countWords
            
            # Dictionaries
            posTags=preprocessedItem.fullText.getPosDictionary(includeStopwords=includeStopwords)
            posTagsRelative=preprocessedItem.fullText.getPosDictionary(includeStopwords=includeStopwords, relative=True)
            posTagClasses=preprocessedItem.fullText.getPosClassesDictionary(includeStopwords=includeStopwords)
            posTagClassesRelative=preprocessedItem.fullText.getPosClassesDictionary(includeStopwords=includeStopwords, relative=True)

            taxMatches={}
            # Taxonomie (Knowledge based Feature)
            for taxLevel in self.tax.taxonomieLevels:
                matches=0
                for word in preprocessedItem.fullText.getWords():
                    for keyword in taxLevel.keywordStemps:
                        #if word.posTagClass=="Verb":
                        if keyword==word.lemma:
                            matches+=1
                taxMatches[taxLevel.level]=matches
            taxMatchesRelative=None
            if(countWords>0):
                taxMatchesRelative=counts={k: v / countWords for k, v in taxMatches.items()}
            else:
                taxMatchesRelative=counts={k: v / 1 for k, v in taxMatches.items()}


            featuredItem=FeaturedItem(itemType=itemType, ealidid=id, level=level, words=words, questionWords=wordsQuestion, descriptionWords=wordsDescritpion,
                verbs=verbs, nouns=nouns, questionVerbs=verbsQuestion, descriptionVerbs=verbsDescription,
                questionNouns=nounsQuestion, descriptionNouns=nounsDescription, countSentences=countSentences, countQuestionSentences=countSentencesQuestion, 
                countDescriptionSentences=countSentencesDescription, countToken=countWords, 
                countQuestionToken=countWordsQuestion, countDescriptionToken=countWordsDescription, tokenPerSentence=tokenPerSentence,
                tokenPerSentenceDescription=tokenPerSentenceDescription, tokenPerSentenceQuestion=tokenPerSentenceQuestion,
                countVerbs=countVerbs, countQuestionVerbs=countVerbsQuestion, countDescriptionVerbs=countVerbsDescription,
                countNouns=countNouns, countQuestionNouns=countNounsQuestion, countDescriptionNouns=countNounsDescription,
                keywordmatches=taxMatches, posDictionary=posTags, posClassDictionary=posTagClasses,
                posClassRelativeDictionary=posTagClassesRelative, posRelativeDictionary=posTagsRelative,
                keywordmatchesRelative=taxMatchesRelative, nounsPerToken=nounsPerSentence, verbsPerToken=verbsPerToken,
                verbsPerSentence=verbsPerSentence, nounsPerSentence=nounsPerSentence, testbed=testbed)
            #print(featuredItem["countToken"])
            return(featuredItem)
        else:
            raise Exception("Item have to be Item type") 

    
    def processText(self, input):

        outout=input

    def removePhrases(self, input):
        output=input
        return output