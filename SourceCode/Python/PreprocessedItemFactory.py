from Item import *
from PreprocessedTextFactory import *
class PreprocessedItemFactory():
    def __init__(self, name=""):
        self.name=name
        self.preprocessedTextFactory=PreprocessedTextFactory(stopwordPath=".\\ressources\\stopwords-de.txt")
    def tagItem(self, item):
        points=0
        # Wenn das Item vom Typ Item ist 
        if isinstance(item, Item):
            try:
                points=int(item.points)
            except:
                points=0
            itemType=item.itemType
            itemLevel=item.level
            taggedQuestion=self.preprocessedTextFactory.proceedText(item.question)
            taggedDescription=self.preprocessedTextFactory.proceedText(item.description)
            taggedText=self.preprocessedTextFactory.proceedText(item.question+" "+item.description)
            return(PreprocessedItem(question=taggedQuestion, description=taggedDescription, item=item, ealidid=item.id, fullText=taggedText, itemType=itemType,level=itemLevel, testbed=item.testbed))


        
