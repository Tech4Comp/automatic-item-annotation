class Item():
    def __init__(self, itemType="", id="", title="", description="", question="", points="", level="", minnumber=0, maxnumber=0, testbed=""):
        self.itemType=itemType
        self.id=id
        self.title=title
        self.description=description
        self.question=question
        self.points=points
        self.level=level
        self.minnumber=minnumber
        self.maxnumber=maxnumber
        self.testbed=testbed
    def __str__(self):
        return self.id+"\n\t"+"Klasse: "+str(self.level)+"\n\t"+self.question+" "+self.description+"\n"

class FeaturedItem():
    def __init__(self, level=0, itemType="", ealidid="", words="", questionWords="", descriptionWords="", verbs="",
        nouns="", questionVerbs="", descriptionVerbs="", questionNouns="", descriptionNouns="",
        countSentences=0, countQuestionSentences=0, countDescriptionSentences=0,
        countToken=0, countQuestionToken=0, countDescriptionToken=0, countVerbs=0, countQuestionVerbs=0,
        countDescriptionVerbs=0, countNouns=0, countQuestionNouns=0, countDescriptionNouns=0, tokenPerSentence=0.0,
        tokenPerSentenceDescription=0.0, tokenPerSentenceQuestion=0.0, keywordmatches={}, keywordmatchesRelative={}, posDictionary={},
        posClassDictionary={}, posClassRelativeDictionary={}, posRelativeDictionary={}, nounsPerToken=0.0, verbsPerToken=0.0,
        verbsPerSentence=0.0, nounsPerSentence=0.0, testbed="" ):
        
        self.level=level

        self.itemType=itemType
        self.id=ealidid
        self.testbed=testbed

        self.words=words
        self.questionWords=questionWords
        self.descriptionWords=descriptionWords
        self.verbs=verbs
        self.nouns=nouns
        self.questionVerbs=questionVerbs
        self.questionNouns=questionNouns
        self.descriptionNouns=descriptionNouns
        self.descriptionVerbs=descriptionVerbs
        
        self.countSentences=countSentences
        self.countQuestionSentences=countQuestionSentences
        self.countDescriptionSentences=countDescriptionSentences

        self.countToken=countToken
        self.countQuestionToken=countQuestionToken
        self.countDescriptionToken=countDescriptionToken

        self.countVerbs=countVerbs
        self.countQuestionVerbs=countQuestionVerbs
        self.countDescriptionVerbs=countDescriptionVerbs

        self.countNouns=countNouns
        self.countQuestionNouns=countQuestionNouns
        self.countDescriptionNouns=countDescriptionNouns

        self.tokenPerSentence=tokenPerSentence
        self.tokenPerSentenceQuestion=tokenPerSentenceDescription
        self.tokenPerSentenceDescription=tokenPerSentenceDescription

        self.nounsPerToken=nounsPerSentence
        self.verbsPerToken=verbsPerToken
        self.verbsPerSentence=verbsPerSentence
        self.nounsPerSentence=nounsPerSentence

        self.keywordmatches=keywordmatches
        self.keywordmatchesRelative=keywordmatchesRelative

        self.posDictionary=posDictionary
        self.posClassDictionary=posClassDictionary
        
        self.posRelativeDictionary=posRelativeDictionary
        self.posClassRelativeDictionary=posClassRelativeDictionary

    def __getattribute__(self, name):
        return super().__getattribute__(name)
    
    def __str__(self):
        return str("Featured Item{id:"+str(self.id)+", countSentences:"+str(self.countSentences)+", verbsPerSentence:"+str(self.verbsPerSentence)+", itemType:"+self.itemType+", countVerbs:"+str(self.countVerbs)+", ...}")

class PreprocessedItem():
    #itemStatistic=""
    def __init__(self, question="", description="", item="", ealidid="", fullText="", itemType="", level="", testbed=""):
        self.question=question
        self.description=description
        self.fullText=fullText
        self.ealidid=ealidid
        self.item=item
        self.itemType=itemType
        self.level=level
        self.matches=""
        self.testbed=testbed
    def setItemStatistic(self, itemStatistic):
        self.itemStatistic=itemStatistic
    def setMatches(self, matches):
        self.matches=matches
    #def __str__(self):
    #    return str("ID:"+str(self.ealidid)+"\n Match: "+self.matches +"\n") + "\n Frage: "+str(self.question)+" \n"+"Beschreibung: "+str(self.description)+"\n"+str(self.itemStatistic)+"\n"
    def __str__(self):
        return str(self.fullText)
class PreprocessedText():
    def __init__(self, preprocessedSentence="", nouns="", verbs="", textStatistic=""):
        self.preprocessedSentences=preprocessedSentence
        self.textStatistic=textStatistic
        #self.nouns=nouns
        #self.verbs=verbs
    def __str__(self):
        sentences=""
        i=0
        for sentence in self.preprocessedSentences:
            i=i+1
            sentences=sentences+"\n"+str(i)+". Satz: \t"+str(sentence)
        sentences+=str(self.textStatistic)
        return sentences

    def countToken(self, includeStopwords=True, wordClass="All"):
        count=0
        for sentence in self.preprocessedSentences:
            for word in sentence.taggedWords:
                if wordClass=="All" or wordClass==word.posTagClass:
                    if includeStopwords or not word.stopword:
                        count+=1
        return count
    def getWordsAsString(self, wordClass="All", includeStopwords=True, returnStemps=False):
        output=""
        for sentence in self.preprocessedSentences:
            for word in sentence.taggedWords:
                if wordClass=="All" or wordClass==word.posTagClass:
                    if includeStopwords or not word.stopword:
                        if returnStemps:
                            output+=word.lemma+" "
                        else:
                            output+=word.word+" "
        return output
    def getWords(self):
        words=[]
        for sentence in self.preprocessedSentences:
            for word in sentence.taggedWords:
                words.append(word)
        return words
    def plainText(self):
        sentences=""
        for sentence in self.preprocessedSentences:
            sentences=sentences+str(sentence.sentence)
        return sentences
    
    def getPosDictionary(self, relative=False, includeStopwords=True):
        from collections import Counter
        tags=[]
        for word in self.getWords():
            if includeStopwords or not word.stopword:
                tags.append(word.posTag)
        counts={}
        for tag in tags:
            counts[tag]=counts.get(tag, 0)+1
        if relative:
            counts={k: v / self.countToken(includeStopwords=includeStopwords) for k, v in counts.items()}   
        return counts

    def getPosClassesDictionary(self, relative=False, includeStopwords=True):
        from collections import Counter
        tags=[]
        for word in self.getWords():
            if includeStopwords or not word.stopword:
                tags.append(word.posTagClass)
        counts={}
        for tag in tags:
            counts[tag]=counts.get(tag, 0)+1
        if relative:
            counts={k: v / self.countToken(includeStopwords=includeStopwords) for k, v in counts.items()}
        return counts

class PreprocessedSentence():
    def __init__(self, sentence="", taggedWords="", nouns="", verbs=""):
        self.sentence=sentence
        self.taggedWords=taggedWords
    def __str__(self):
        words=""
        for taggedWord in self.taggedWords:
            words=words+str(taggedWord)
        return ("Satz: \n"+self.sentence+"Wörter: \t"+words+"\n")

class TaggedWord():
    def __init__(self, word, position, posTag, posTagClass, lemma, stopword):
        self.word=word
        self.position=position
        self.posTag=posTag
        self.lemma=lemma
        self.posTagClass=posTagClass
        self.stopword=stopword
    def __str__(self):
        return("Wort: \t"+self.word+" Position: \t"+str(self.position)+"Tag Klasse: \t"+self.posTagClass+" PoSTag: \t"+self.posTag+" Lemma: \t"+self.lemma+"\n")
    def getWord(self):
        return(self.word)
    def getLemma(self):
        return(self.lemma)
    def __repr__(self):
        return self.getWord()
class SentenceStatistic():
    def __init__(self, countWords, countPosTags, countLemma):
        self.countWords=countWords
        self.countPosTags=countPosTags
        self.countLemma=countLemma
    def __str__(self):
        return("Satzstatistik: \t Anzahl Wörter: "+ str(self.countWords)+"\t PoSTags: "+str(self.countPosTags)+"\t Lemmas: "+str(self.countLemma))

class TextStatistic():
    def __init__(self, countWords=0, countSentences=0, countPosTags="", countLemma=""):
        self.countWords=countWords
        self.countSentences=countSentences
        self.countPosTags=countPosTags
        self.countLemma=countLemma
    def __str__(self):
        return("\n Textstatistik: \t Anzahl Wörter: "+ str(self.countWords)+"\t Anzahl Sätze: "+str(self.countSentences)+"\t PoSTags: "+str(self.countPosTags)+"\t Lemmas: "+str(self.countLemma))

class ItemStatistic():
    def __init__(self, countWords=0, countSentences=0, countPosTags="", countLemma=""):
        self.countWords=countWords
        self.countSentences=countSentences
        self.countPosTags=countPosTags
        self.countLemma=countLemma
    def __str__(self):
        return("\n Item Statistik: \t Anzahl Wörter: "+ str(self.countWords)+"\t Anzahl Sätze: "+str(self.countSentences)+"\t PoSTags: "+str(self.countPosTags)+"\t Lemmas: "+str(self.countLemma))
