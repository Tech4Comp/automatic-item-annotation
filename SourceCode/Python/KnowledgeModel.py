import xlrd
from KeywordDictionary import KeywordDirctionary, PerformanceLeveToInt
class KnowlegeModel():
    def __init__(self, pathToModel):
        self.pathToModel=pathToModel
        self.rules=[]
        self.initValues={}
        self.initModel()

    def initModel(self):
    # Opening Workbook
        wb=xlrd.open_workbook(self.pathToModel)
        rulesSheet=wb.sheet_by_name("Regeln")
        rulesHeader=[]
        # Get Rules
        for i in range(rulesSheet.nrows):
            if i==0:
                for j in range(rulesSheet.ncols):
                    rulesHeader.append(rulesSheet.cell_value(i,j))
            else:
                rule={}
                for j in range(rulesSheet.ncols):         
                    rule[rulesHeader[j]]=rulesSheet.cell_value(i,j)
                self.rules.append(rule)
        # Get Init Values
        initSheet=wb.sheet_by_name("Initialisierung")
        testbedDict={}
        initHeader=[]
        initValues={}
        for i in range(initSheet.ncols):
            if i>0:
                testbedDict[i]=initSheet.cell_value(0,i)
                self.initValues[initSheet.cell_value(0,i)]={}
        #print(initValues)
        for i in range(initSheet.nrows):
            if i>0:
                init={}
                level=initSheet.cell_value(i,0)  
                for j in range(initSheet.ncols):
                    if j>0:
                        if testbedDict.get(j)==initSheet.cell_value(0,j):
                            init[level]=initSheet.cell_value(i,j)                   
                            self.initValues[testbedDict.get(j)].update(init)
                            init={}

                    #print(self.initValues[initSheet.cell_value(0,j)])
        #raise NameError()
        
    def categorizeItem(self, featuredItem):
        #Initialisierung eines Gewichtsvektors
        weight={}
        weight["wissen"]=0
        weight["verstehen"]=0
        weight["anwenden"]=0
        weight["analyse"]=0
        weight["synthese"]=0
        weight["evaluation"]=0
        # Initialisiere Werte, wenn in Wissensbasis angegeben
        for testbed, values in self.initValues.items():
            #print(str(self.initValues))         
            #print(featuredItem.testbed)
            if featuredItem.testbed==testbed:
                for taxlevel, value in values.items():
                    weight[taxlevel.lower()]+=value
        i=0
        appliedRules=[]
        #Itteriere durch alle Regeln
        for rule in self.rules:
            #print(rule)
            clause=""
            pra=rule["Prämisse"].lower()
            # Splite am 'und' in der Prämisse, da dies aber für die Wissenbasis nicht nötig ist, wird 'und' nicht beachtet!
            elements=pra.split("und")
            for element in elements:
                # Verarbeite Keyword Rules
                if "keyword" in element:
                    words=element.split(" aus ")
                    searched=(words[1].split(" "))[0]
                    matches=featuredItem.keywordmatches.get(KeywordDirctionary.keywords.get(searched))
                    if matches>0:
                        consequences=rule["Konsequenz"]
                        parts=rule["Konsequenz"].split(" um ")
                        faktor=0
                        if len(parts)>1:
                            faktor=parts[1]
                        else:
                            faktor=1
                        target=parts[0].split(" ")
                        weight[target[1].lower()]+=(matches*int(faktor))
                        appliedRules.append(rule)
                    #components=rule["Konsequenz"].split("und")
                    #for coponent in components:
                      #  parts=rule["Konsequenz"].split(" um ")
                     #   faktor=0
                      #  if len(parts)>1:
                       #     faktor=parts[1]
                       # else:
                        #    faktor=1
                       # print(featuredItem.keywordmatches.items())
                        #raise NameError

                        #for keyword, value in featuredItem.keywordmatches.items():
                         #   print(keyword+" "+str(value))
                          #  element=KeywordDirctionary.keywords.get(keyword)
                           # #print(element)
                            #weight[element]+=(value*int(faktor))
                            #if value>0:
                            #    appliedRules.append(rule)
                            #print(weight[element])
                # Process Single Value Rules
                if " ist " in element:
                    words=element.split(" ")
                    if words[0] != "":
                        prop=featuredItem.__getattribute__(KeywordDirctionary.keywords.get(words[0])).lower()
                        #print(words[2])
                        #print(KeywordDirctionary.keywords.get(words[2])+" "+prop)
                        if prop.lower()==KeywordDirctionary.keywords.get(words[2]):
                            #print("match")
                            appliedRules.append(rule)
                            konsequenz=rule["Konsequenz"]#.lower().split( " und " )
                            #for konsequenz in konsequenzes:
                            kons_words=konsequenz.split(" ")
                                #print(konsequenz)
                            if len(kons_words)==4:
                                if kons_words[0]=="erhöhe":
                                    element=KeywordDirctionary.keywords.get(kons_words[1])
                                        #weight[kons_words[1].lower()]+=int(kons_words[3])
                    #print ("abgleich")
                if "gewicht" in element:
                    element=element.replace("gewicht ","")
                    if "am größten" in element:
                        key=element.split(" ")[0]
                        value=weight.get(key)
                        #print(key+" "+str(value))
                        checker=True
                        for level, wei in weight.items():
                            if level.lower()!=key.lower():
                                if wei>value:
                                    checker=False
                        if checker:
                            konsequenz=rule["Konsequenz"].lower()
                            if "annotiere als" in konsequenz:
                                kons=konsequenz.replace("annotiere als ","").lower()
                                appliedRules.append(rule)
                                output={}
                                output["level"]=PerformanceLeveToInt.keywords.get(kons)
                                output["reporting"]=appliedRules
                                print("Angewandte Regeln: "+str(appliedRules))
                                #raise NameError
                                return(output)


                


    def __str__(self):
        return str(self.rules)+" "+str(self.initValues)

    
    