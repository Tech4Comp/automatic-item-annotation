CREATE TABLE IF NOT EXISTS `algorithms` (
	`name` VARCHAR(50) NOT NULL DEFAULT '',
	PRIMARY KEY (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;
CREATE TABLE IF NOT EXISTS `features` (
	`name` VARCHAR(50) NOT NULL DEFAULT '',
	`type` VARCHAR(50) NULL DEFAULT NULL,
	`relative` INT(11) NULL DEFAULT NULL,
	`category` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`name`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE `resultsets` (
	`RS_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`level` VARCHAR(50) NULL DEFAULT '',
	`precision` DECIMAL(20,10) NULL DEFAULT NULL,
	`recall` DECIMAL(20,10) NULL DEFAULT NULL,
	`f1` DECIMAL(20,10) NULL DEFAULT NULL,
	`accuracy` DECIMAL(20,10) NULL DEFAULT NULL,
	`R_ID` INT(11) NOT NULL,
	PRIMARY KEY (`RS_ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=1
;
CREATE TABLE `runs` (
	`R_ID` INT(11) NOT NULL AUTO_INCREMENT,
	`accuracy` DECIMAL(10,5) NULL DEFAULT '0',
	`fitTime` DECIMAL(20,10) NULL DEFAULT '0',
	`scoreTime` DECIMAL(20,10) NULL DEFAULT '0',
	`confusionMatrix` VARCHAR(500) NULL DEFAULT '0',
	`algorithm` VARCHAR(50) NULL DEFAULT NULL,
	`application` VARCHAR(50) NULL DEFAULT '',
	`runid` INT(11) NULL DEFAULT NULL,
	`C_ID` INT(11) NULL DEFAULT NULL,
	PRIMARY KEY (`R_ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
AUTO_INCREMENT=67
;



CREATE TABLE `runsfeatures` (
	`R_ID` INT(11) NULL DEFAULT NULL,
	`name` VARCHAR(50) NULL DEFAULT NULL
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE TABLE IF NOT EXISTS `textprocessingoptions` (
	`C_ID` INT(11) NOT NULL DEFAULT '0',
	`stemps` TINYINT(1) NOT NULL DEFAULT '0',
	`stopwords` TINYINT(1) NOT NULL DEFAULT '1',
	PRIMARY KEY (`C_ID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB
;

CREATE OR REPLACE VIEW makro_avgs AS (
	SELECT runs.*, AVG(resultsets.`precision`) AS makro_precision, AVG(resultsets.f1) AS makro_f1, AVG(resultsets.recall) AS makro_recall FROM
	runs JOIN resultsets ON runs.R_ID=resultsets.R_ID WHERE ALGORITHM NOT LIKE '%linear svc%'
	GROUP BY resultsets.R_ID 
);
