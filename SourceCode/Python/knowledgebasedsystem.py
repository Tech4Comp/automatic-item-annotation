# User definied classes
import KnowledgeModel as knmo
from TextFeatureFactory import *
from PreprocessedItemFactory import *
from CsvImporter import *
from Item import *
from ConfigReader import *
from JsonImporter import *
import numpy as np
# Location
loc='ressources\\rules.xlsx'
km= knmo.KnowlegeModel(loc)

# Config einlesen
configReader=ConfigReader("config.xml")
config=configReader.readConfig()
preprocessingCombinations=config.getPreprocessing()
featureCombinations=config.getFeatures()
dataMiningMethodes=config.getDMMethods()
classificationTarget=config.getClassificationTarget()
itemData=config.getItemSets()
dbData=config.getDbData()

# Logger 
reporter=open("ressources\\report.txt","w",encoding="utf-8")

# Initialize global Variables
taggedItemFactory=PreprocessedItemFactory()
textFeatureFactory=TextFeatureFactory()
jsonImporter=JsonImporter()
results=[]


def confMatrixToReport(confMatrix):
    #print(confMatrix)
    scores={}
    scores["resultsets"]=[]
    size=len(confMatrix)
    level=1
    samplesize=0
    correct=0
    for line in confMatrix:
        for field in line:
            samplesize+=field
    # Add True Positive
    while level<=size:
        score={}
        score["tp"]=confMatrix[level-1][level-1]
        correct+=score["tp"]
        score["fp"]=0
        score["fn"]=0
        score["tn"]=0
        score["support"]=0
        for i, line in enumerate(confMatrix, start=1):
            if i==level:
                for j, number in enumerate(line, start=1):
                    
                    #print(index)
                    if j!=level:
                        score["fp"]+=number
            else:         
                score["fn"]+=line[level-1]
                for j, number in enumerate(line, start=1):
                    #print(index)
                    if j!=level:
                        score["tn"]+=number
        # calculate precision and recall       
        if  score["tp"]!=0:      
            score["precision"]=score["tp"]/(score["tp"]+score["fp"])
            score["recall"]=score["tp"]/(score["tp"]+score["fn"])
        else:
            score["precision"]=0
            score["recall"]=0
        
        if score["tp"]!=0 or score["tn"]!=0:
            score["accuracy"]=(score["tp"]+score["tn"])/(score["tp"]+score["fp"]+score["tn"]+score["fn"])
        else:
            score["accuracy"]=0

        if score["precision"]!=0 or score["recall"]!=0:
            score["f1"]=2*((score["precision"]*score["recall"])/(score["precision"]+score["recall"]))
        else:
            score["f1"]=0
        score["level"]=level
        scores["resultsets"].append(score)
        level+=1
    scores["accuracy"]=correct/samplesize
    #print(scores)
    return scores

## Methods for processing
def generateItemsFromCsvRaw(path, testbed=""):
    items=[]
    # CSV importieren
    csvImporter=CsvImporter(inFile=path)
    csvImporter.readFile()
    # create Items from CSV import
    for data in csvImporter.dataSet:
        data=csvImporter.dataSet[data]
        # Create Item Object
        item=Item.Item(question=data.get("question",""), description=data.get("description",""), id=data.get("id",""),\
            itemType=data.get("type",""), points=data.get("points",""), level=data.get("level","0"), minnumber=data.get("minnumber",0), maxnumber=data.get("maxnumber",0),
            testbed=testbed)
        # Add to Items Array
        items.append(item)
    return items

def generatePreprocessedItemsFromItems(items):
    taggedItems=[]
    for item in items:
        taggedItem=taggedItemFactory.tagItem(item)
        taggedItems.append(taggedItem)
    return taggedItems

def generateTargetArrayFromFeaturedItems(featuredItems):
    targedClasses=[]
    for featuredItem in featuredItems:
        targedClasses.append(featuredItem.__getattribute__(classificationTarget))
    return targedClasses

def generateFeaturedItemFromPreprossesedItems(preprocessedItems, returnStemps, includeStopwords):
    featuredItems=[]
    for preprocessedItem in preprocessedItems:
            featuredItem=textFeatureFactory.proceedItem(preprocessedItem, 
                includeStopwords=includeStopwords, returnStemps=returnStemps)
            featuredItems.append(featuredItem)
    return featuredItems
print(str(km))

itemset=config.getItemSets()[0]
print(itemset)
items=generateItemsFromCsvRaw(itemset.get("path"),itemset["testbed"])
preprocessedItems=generatePreprocessedItemsFromItems(items)
featuredItems=generateFeaturedItemFromPreprossesedItems(preprocessedItems, True, False)


results=[]
targetClasses=[]

for featuredItem in featuredItems:
    output=km.categorizeItem(featuredItem)
    level=output.get("level")
    reporter.write("Item-ID: "+featuredItem.id +"\n" +"Itemtyp: "+featuredItem.itemType)
    reporter.write("\n Angewandte Regeln: \n")
    for rule in output.get("reporting"):
        reporter.write("\t"+str(rule)+"\n")
    reporter.write("\n")
    results.append(level)
    targetClasses.append(featuredItem.level)
    print(str(level)+" "+str(featuredItem.level))

conf_mat = np.zeros((6,6))
i=0
while i<len(results):
    conf_mat[int(results[i])-1,int(targetClasses[i])-1]+=1
    i+=1

print(str(conf_mat))
res=confMatrixToReport(conf_mat)
print(str(res["resultsets"]))