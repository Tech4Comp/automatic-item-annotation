from os import path
from TagDictionary import SttsTagClasses
import xlsxwriter
from Taxonomie import *
from TaxonimieReader import *
from pattern.de import lemma

class StatsExporter():
    def __init__(self, directory="", filename=""):
        self.directory=directory
        self.filename=filename
        self.fullPath=directory+"\\"+filename
        if ".xlsx" not in filename:
            self.fullPath+=".xlsx"
        cr=TaxonimieReader("./ressources/taxonomie.xml")
        self.tax=cr.readTaxonomie()
        print(len(self.tax.taxonomieLevels))
        self.workbook=xlsxwriter.Workbook(self.fullPath)

    def close(self):
        self.workbook.close()

    def printItemStats(self, sheetname="", preprocessedItems=None):
        worksheet=self.workbook.add_worksheet(sheetname)
        # Header
        worksheet.write(0,0, "ID")
        worksheet.write(0,1, "Token")
        worksheet.write(0,2, "Sentences")
        worksheet.write(0,3, "Stopwords")
        row=0
        column=4
        processed=[]
        # Header for POS Tags
        for key, value in SttsTagClasses.sttsTagClasses.items():       
            print(processed)
            print(value)
            if value not in processed:
                worksheet.write(row, column, value)
                processed.append(value)
                column=column+1
        # Header for Keyword Matches
        for taxLevel in self.tax.taxonomieLevels:
            print(str(taxLevel))
            worksheet.write(row, column, taxLevel.level)
            column=column+1
        column=0
        maxColumns=0
        row=1
        for preprocessedItem in preprocessedItems:
            worksheet.write(row,column, preprocessedItem.ealidid)
            column=column+1
            countToken=preprocessedItem.fullText.countToken(includeStopwords=True, wordClass="All")
            worksheet.write(row,column, countToken)
            column=column+1
            countSentences=len(preprocessedItem.fullText.preprocessedSentences)
            worksheet.write(row,column, countSentences)
            column=column+1
            countStopword=countToken-preprocessedItem.fullText.countToken(includeStopwords=False, wordClass="All")
            worksheet.write(row,column, countStopword)
            column=column+1
            processed=[]
            # Wortarten
            for key, value in SttsTagClasses.sttsTagClasses.items():
                if value not in processed:
                    countWords=preprocessedItem.fullText.countToken(includeStopwords=True, wordClass=value)
                    worksheet.write(row,column,countWords)
                    processed.append(value)
                    column=column+1
            # Keyword Matches
            for taxLevel in self.tax.taxonomieLevels:
                matches=0
                for word in preprocessedItem.fullText.getWords():
                    for keyword in taxLevel.keywordStemps:
                        if word.posTagClass=="Verb":
                            if keyword==lemma(word.word):
                                matches+=1
                worksheet.write(row,column,matches)
                column=column+1
            column=0
            row=row+1
        