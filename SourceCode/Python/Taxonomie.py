from pattern.de import lemma
class Taxonomie():
    def __init__(self, taxonomieLevel=[]):
        self.taxonomieLevels=[]

    def addLevelToTaxonomie(self, taxonomieLevel):

        self.taxonomieLevels.append(taxonomieLevel)
        #print(str(self))
    def __str__(self):
        output=""
        for taxlevel in self.taxonomieLevels:
            output+=str(taxlevel)
        return output
    
class TaxonomieLevel():
    def __init__(self, level, keywords):
        self.level=level
        self.keywords=keywords
        self.keywordStemps=self.stemmKeyWords()

    def stemmKeyWords(self):
        keywordStemps=[]
        for keyword in self.keywords:
            keywordStemps.append(lemma(keyword))
        return keywordStemps

    def __str__(self):
        return self.level+": "+str(self.keywords)+ "("+str(self.keywordStemps) +")\n"