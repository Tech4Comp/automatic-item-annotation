import json
import Item

class JsonImporter():
    def __init__(self):
        self.name="JSON Importer"

    def importJson(self, path, mode):
        fd=open(path, encoding="UTF8")
        jsonInput=fd.read()
        fd.close()
        decoder=json.JSONDecoder()
        obj=decoder.decode(jsonInput)
        if mode=="preprocessedItems":
            print("Importing preprocessesed Items")
            items=[]
            for item in obj:
                preprocessedItem=Item.PreprocessedItem()
                preprocessedItem.ealidid=item.get("id")
                preprocessedItem.itemType=item.get("type")
                preprocessedItem.level=item.get("level")
                # Read in question
                questionStr=item.get("question")
                questionSentences=[]
                fullTextSentences=[]
                for preprocessedSentence in questionStr.get("taggedSentences"):
                    questionSentence=Item.PreprocessedSentence()
                    questionSentence.sentence=preprocessedSentence.get("sentence")
                    taggedWords=[]
                    for taggedWord in preprocessedSentence.get("taggedWords"):
                        word=Item.TaggedWord(taggedWord["word"], taggedWord["position"], taggedWord["posTag"],
                            taggedWord["posTagClass"],taggedWord["lemma"], taggedWord["stopword"])
                        taggedWords.append(word)
                    questionSentence.taggedWords=taggedWords
                    questionSentences.append(questionSentence)
                    fullTextSentences.append(questionSentence)
                preprocessedItem.question=Item.PreprocessedText(preprocessedSentence=questionSentences)
                # Read in description
                descriptionStr=item.get("description")
                descriptionSentences=[]
                for preprocessedSentence in descriptionStr.get("taggedSentences"):
                    descriptionSentence=Item.PreprocessedSentence()
                    descriptionSentence.sentence=preprocessedSentence.get("sentence")
                    taggedWords=[]
                    for taggedWord in preprocessedSentence.get("taggedWords"):
                        word=Item.TaggedWord(taggedWord["word"], taggedWord["position"], taggedWord["posTag"],
                            taggedWord["posTagClass"],taggedWord["lemma"], taggedWord["stopword"])
                        taggedWords.append(word)
                    descriptionSentence.taggedWords=taggedWords
                    descriptionSentences.append(descriptionSentence)
                    fullTextSentences.append(descriptionSentence)
                preprocessedItem.description=Item.PreprocessedText(preprocessedSentence=descriptionSentences)
                preprocessedItem.fullText=Item.PreprocessedText(preprocessedSentence=fullTextSentences)
                items.append(preprocessedItem)
            return items

