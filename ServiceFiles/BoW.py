import os
import json
from joblib import load
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.tree import DecisionTreeClassifier
# Turn off scikit warnings
def warn(*args, **kwargs):
    pass
import warnings

# Bag of Words Model laden
bow=load('Process/ressources/BoW.joblib') 

# JSON laden und in Objekt umwandeln
fd=open('Process/ressources/TestItem.json', encoding="UTF8")
jsonInput=fd.read()
fd.close()
decoder=json.JSONDecoder()
obj=decoder.decode(jsonInput)

# Bag of Words auf Text anwenden, dazu das Modell BoW Model laden (Vokabular)
vectorizer= CountVectorizer(vocabulary=load('Process/ressources/BoWvocabulary.joblib'), max_features=322)
x=vectorizer.fit_transform([obj["text"]])

# Predict target class
prediction=bow.predict(x)

print(prediction)
