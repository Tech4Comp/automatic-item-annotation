import os
import json
from collections import Counter
# Importing and exporting Models
from joblib import load
# Datamining
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.tree import DecisionTreeClassifier
# Preprocessing
from pattern.de import parse, split, tag, pprint, lemma, tokenize, parsetree

# Turn off scikit warnings
def warn(*args, **kwargs):
    pass
import warnings

# Bag of Words Model laden
pos=load('Process/ressources/pos.joblib') 

# JSON laden und in Objekt umwandeln
fd=open('Process/ressources/TestItem.json', encoding="UTF8")
jsonInput=fd.read()
fd.close()
decoder=json.JSONDecoder()
obj=decoder.decode(jsonInput)
# Unfortunataly there is no init for dict vectorizer. insted use BoF with PoS-Tags
# Load model
vectorizer= CountVectorizer(vocabulary=load('Process/ressources/posdict.joblib'), max_features=322)

# Preprocessing
# Sentenice
sentences=tokenize(obj["text"], punctuation=".,;:!?()[]{}`''\"@#$^&*+-|=~_", replace={})
# Tokenize and PoS - store only tags in array
tags=""
for sentence in sentences:
    tokens=tag(sentence, tagset="STTS")
    for word, posTag in tokens:
        tags+=posTag+" "
# Convert array into dict (tag: count)
x=vectorizer.fit_transform([tags])
# Predict taget class
prediction=pos.predict(x)
print(prediction)