#!/usr/bin/gnuplot

reset
#set terminal pngcairo size 700,480
#set terminal svg
set terminal pdfcairo size 5,4
#set terminal dumb
set output "ml_algorithms_biwi.pdf"

set datafile separator ";"

set style line 1 linecolor rgb "#F4511E" linewidth 3 pointtype 2 pointsize 1 #orange
set style line 2 linecolor rgb "#43A047" linewidth 1 pointtype 5 pointsize 1 #green
set style line 3 linecolor rgb "#1E88E5" linewidth 3 pointtype 6 pointsize 1 #blue

#set border 2
set grid back ls 1 lc rgb '#707070' lw 0.7
set grid ytics mytics
set mytics 2
set style line 11 lc rgb '#707070' lt 1 lw 1
set border back ls 11
set xtics nomirror scale 0 font ",16" rotate by 0
set ytics scale 1 font ",16"
set xrange [0.5:6.5]
set yrange[0:1]
set xlabel 'Algorithms' font ",16" offset 0,-0.5
set ylabel 'F1 score' font ",16"
set bmargin at screen 0.35

set key at graph 0.7, 0.19 font ",14"

plot 'ml_algorithms_biwi.csv' using 2:5 ls 1 with yerrorbars title "Average with standard deviation",\
     '' using 0:4:xticlabels(1) ls 2 with point title "Maximum",\
     '' using 0:3 ls 3 with point title "Minimum"
