#!/usr/bin/gnuplot

reset
#set terminal pngcairo size 700,480
#set terminal svg
set terminal pdfcairo size 5,4
#set terminal dumb
set output "amount_attributes_dbs.pdf"

set datafile separator ";"

set style line 1 linecolor rgb "#F4511E" linewidth 3 pointtype 2 pointsize 1 #orange
set style line 2 linecolor rgb "#43A047" linewidth 1 pointtype 5 pointsize 1 #green
set style line 3 linecolor rgb "#1E88E5" linewidth 3 pointtype 6 pointsize 1 #blue
set style line 4 linecolor rgb "#5E35B1" linewidth 2 pointtype 8 pointsize 1 #purple

#set border 2
set grid back ls 1 lc rgb '#707070' lw 0.7
set grid ytics mytics
set mytics 3
set style line 11 lc rgb '#707070' lt 1 lw 1
set border back ls 11
set xtics nomirror scale 0 font ",16"
set ytics scale 1 font ",16"
set xrange [0.5:4.5]
set yrange[0:1]
set xlabel 'Amount of item features' font ",16" offset 0
set ylabel 'F1 score' font ",16"
set bmargin at screen 0.4

set key at graph 1.0, 0.355 font ",14"

f(x) = log(x**n)+m
fit f(x) 'amount_attributes_dbs.csv' using 0:2 via n,m

plot 'Anzahl_Attribute.csv' using 2:5 ls 1 with yerrorbars title "Average with standard deviation",\
     f(x) ls 4 with lines title 'log(x^{0.13})+0.46',\
     '' using 0:4:xticlabels(1) ls 2 with point title "Maximum",\
     '' using 0:3 ls 3 with point title "Minimum"
