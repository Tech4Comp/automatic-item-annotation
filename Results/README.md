The evaluated results of all measurements are stored in the two xlsx files. **\_dbs** is the computer science data set. **\_biwi** is the educational data set.

Gnuplot is used for plotting specific tables of the results. To recreate the plots, export a specific table form the xlsx file in the csv format, name it as specified in the gnuplot scripts, if needed resolve format issues (e.g. exchange `0,` with `0.`) and execute the script to create a visual plot of the results.
